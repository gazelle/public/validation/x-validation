package net.ihe.gazelle.xvalidation.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.preferences.PreferenceService;

import org.jboss.seam.security.Identity;

public enum XValidationAuthorizations implements Authorization {


	ALL,

	LOGGED,

	ADMIN,

	TEST_DESIGNER,

	VALIDATOR,

	EDITOR;

	@Override
	public boolean isGranted(Object... context) {
		boolean enabled = PreferenceService.getBoolean("x_validation_enabled");
		if (!enabled) {
			return false;
		} else {
			boolean userIsLoggedIfNeeded = !PreferenceService.getBoolean("user_need_to_be_logged_in")
					|| Identity.instance().isLoggedIn();
			switch (this) {
			case ALL:
					return true;
			case LOGGED:
				return userIsLoggedIfNeeded;
			case ADMIN:
				return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
			case TEST_DESIGNER:
				return Identity.instance().isLoggedIn()
						&& (Identity.instance().hasRole("admin_role") || Identity.instance().hasRole("tests_editor_role"));
			case EDITOR:
				return PreferenceService.getBoolean("x_validation_editor")
						&& userIsLoggedIfNeeded;
			case VALIDATOR:
				return PreferenceService.getBoolean("x_validation_validator")
						&& userIsLoggedIfNeeded;
			default:
				return false;
			}
		}
	}

}
