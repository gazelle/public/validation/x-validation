package net.ihe.gazelle.xvalidation.input.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.model.MessageType;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.ReferencedObjectDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("validatorInputEditorManager")
@Scope(ScopeType.PAGE)
public class ValidatorInputEditorManager implements Serializable {

    private transient GazelleCrossValidatorType validator;
    private transient Integer validatorId;
    private ValidatorInput newValidatorInput;
    private String selectedKeyword;
    private String oldKeyword;
    private Boolean existingReferencedObjectSelected;

    private static final List<SelectItem> OBJECT_TYPES;
    private static final List<SelectItem> MSG_TYPES;


    static {
        OBJECT_TYPES = new ArrayList<SelectItem>();
        MSG_TYPES = new ArrayList<SelectItem>();
        for (Kind kind : Kind.values()) {
            OBJECT_TYPES.add(new SelectItem(kind, kind.getValue()));
        }
        for (MessageType msgtype : MessageType.values()) {
            MSG_TYPES.add(new SelectItem(msgtype, msgtype.getValue()));
        }
    }

    public ValidatorInput getNewValidatorInput() {
        return newValidatorInput;
    }

    public void setNewValidatorInput(ValidatorInput newValidatorInput) {
        this.newValidatorInput = newValidatorInput;
    }

    public String getSelectedKeyword() {
        return selectedKeyword;
    }

    public void setSelectedKeyword(String selectedKeyword) {
        this.selectedKeyword = selectedKeyword;
    }

    public Boolean getExistingReferencedObjectSelected() {
        return existingReferencedObjectSelected;
    }

    public void setExistingReferencedObjectSelected(Boolean existingReferencedObjectSelected) {
        this.existingReferencedObjectSelected = existingReferencedObjectSelected;
    }

    public List<SelectItem> getObjectTypes() {
        return OBJECT_TYPES;
    }

    public List<SelectItem> getMsgTypes() {
        return MSG_TYPES;
    }


    @Create
    public void initializeValidatorInputEdition() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        validator = null;
        if (params.containsKey("validatorId")) {
            validatorId = Integer.parseInt(params.get("validatorId"));
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().find(validatorId);
            if (validator == null) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The given validatorId does not reference an existing validator");
            }
        }
        if (params.containsKey("id")) {
            Integer inputId = Integer.parseInt(params.get("id"));
            newValidatorInput = ValidatorInputDAO.instanceWithDefaultEntityManager().find(inputId);
            selectedKeyword = newValidatorInput.getKeyword();
            oldKeyword = newValidatorInput.getReferencedObject().getKeyword();
            setExistingReferencedObjectSelected(false);
            if (newValidatorInput == null) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The given id does not reference an existing input");
            }
            validator = newValidatorInput.getGazelleCrossValidator();
        } else if (validator != null) {
            // We created a new input
            newValidatorInput = new ValidatorInput(validator);
        }
        //listAvailableSchemes();
    }

    public void validateReferencedObjectKeyword(FacesContext context, UIComponent component, Object value)
            throws ValidatorException {
        if (!existingReferencedObjectSelected) {
            if (value instanceof String) {
                String keyword = (String) value;
                if (!ReferencedObjectDAO.instanceWithDefaultEntityManager().isKeywordAvailable(keyword, null) && !keyword.equals(oldKeyword)) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Keyword of the input shall be unique accross the application and this one is already used",
                            null));
                }
            }
        }
    }

    public List<String> getReferencedObjectKeywordList() {
        List<ReferencedObject> referencedObjectList = ReferencedObjectDAO.instanceWithDefaultEntityManager().getAllEntities();
        List<String> keywordList = new ArrayList<>();
        for (ReferencedObject referencedObject : referencedObjectList) {
            if (referencedObject != null)  {
                String keyword = referencedObject.getKeyword();
                if (!keyword.isEmpty()) {
                    keywordList.add(keyword);
                }
            }
        }
        return keywordList;
    }

    public void setInputReferencedObject(AjaxBehaviorEvent event) {
        setExistingReferencedObjectSelected(true);
        newValidatorInput.setReferencedObject(ReferencedObjectDAO.instanceWithDefaultEntityManager().getReferencedObjectByKeyword(selectedKeyword));
    }

    public String back() {
        if (validator.getId() != null) {
            return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
        } else {
            return XValidationPages.ADMIN_VALIDATORS.getLink();
        }
    }

    public void saveInput() {
        if (newValidatorInput != null) {
            newValidatorInput = ValidatorInputDAO.instanceWithDefaultEntityManager().saveEntity(newValidatorInput);
            GuiMessage.logMessage(StatusMessage.Severity.INFO, "Input has been saved");
        }
    }
}
