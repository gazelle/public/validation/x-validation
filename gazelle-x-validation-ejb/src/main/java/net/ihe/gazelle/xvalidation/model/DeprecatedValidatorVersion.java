package net.ihe.gazelle.xvalidation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;

import org.hibernate.annotations.Type;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "xval_deprecated_validator_version", schema = "public")
@SequenceGenerator(name = "xval_deprecated_validator_version_sequence", sequenceName = "xval_deprecated_validator_version_id_seq", allocationSize = 1)
public class DeprecatedValidatorVersion implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8598457640727710336L;
	private static final String ARCHIVES_DIR = "/archives";

	@Id
	@GeneratedValue(generator = "xval_deprecated_validator_version_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	private String name;

	private String version;

	@Column(name = "affinity_domain")
	private String affinityDomain;

	@Column(name = "file_path")
	private String filePath;

	@Column(name = "last_modifier")
	private String lastModifier;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified")
	private Date lastModified;


	@Lob
	@Type(type = "text")
	@Column(name = "comment")
	private String comment;

	public DeprecatedValidatorVersion() {

	}

	public DeprecatedValidatorVersion(GazelleCrossValidatorType validator, String filename) {
		this.name = validator.getName();
		this.affinityDomain = validator.getAffinityDomain();
		this.version = validator.getVersion();
		this.filePath = filename;
	}

	@PrePersist
	public void onChange() {
		lastModified = new Date();
		lastModifier = Identity.instance().getCredentials().getUsername();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAffinityDomain() {
		return affinityDomain;
	}

	public void setAffinityDomain(String affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getLastModifier() {
		return lastModifier;
	}

	public void setLastModifier(String lastModifier) {
		this.lastModifier = lastModifier;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((affinityDomain == null) ? 0 : affinityDomain.hashCode());
		result = (prime * result) + ((name == null) ? 0 : name.hashCode());
		result = (prime * result) + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DeprecatedValidatorVersion other = (DeprecatedValidatorVersion) obj;
		if (affinityDomain == null) {
			if (other.affinityDomain != null) {
				return false;
			}
		} else if (!affinityDomain.equals(other.affinityDomain)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (version == null) {
			if (other.version != null) {
				return false;
			}
		} else if (!version.equals(other.version)) {
			return false;
		}
		return true;
	}

	public static String getDirectoryForArchives() {
		String baseDir = PreferenceService.getString("xvalidator_directory");
		baseDir = baseDir.concat(ARCHIVES_DIR);
		return baseDir;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void appendComment(String newComment) {
		if (this.comment == null) {
			setComment(newComment);
		} else {
			StringBuilder builder = new StringBuilder(comment);
			if (!comment.isEmpty()) {
				builder.append("\n");
			}
			builder.append(newComment);
			setComment(builder.toString());
		}
	}
}
