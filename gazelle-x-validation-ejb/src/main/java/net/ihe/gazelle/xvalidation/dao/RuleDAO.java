package net.ihe.gazelle.xvalidation.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.assertion.ws.coverage.provider.data.CoveringEntity;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.provider.CrossValidatorLinkDataProvider;

import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleDAO extends DAO<Rule> {

	private static Logger log = LoggerFactory.getLogger(RuleDAO.class);

	public RuleDAO() {
		super();
	}

	public RuleDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static RuleDAO instanceWithDefaultEntityManager() {
		return new RuleDAO();
	}

	public Rule getRuleByKeyword(String keyword) {
		RuleQuery query = new RuleQuery(entityManager);
		query.keyword().eq(keyword);
		return query.getUniqueResult();
	}

	public List<Rule> getActiveRulesForValidator(GazelleCrossValidatorType validator) {
		RuleQuery query = new RuleQuery(entityManager);
		List<HQLRestriction> restrictions = buildRestrictionListOnParent(query, validator);
		restrictions.add(query.gazelleCrossValidator().id().eqRestriction(validator.getId()));
		query.addRestriction(HQLRestrictions.or(restrictions.toArray(new HQLRestriction[restrictions.size()])));
		query.status().eq(RuleStatus.ACTIVE);
		return query.getList();
	}

	public List<Rule> getRulesFromParents(GazelleCrossValidatorType validator) {
		if (validator.getValidatorParents().isEmpty()) {
			return null;
		} else {
			RuleQuery query = new RuleQuery(entityManager);
			List<HQLRestriction> restrictions = buildRestrictionListOnParent(query, validator);
			query.addRestriction(HQLRestrictions.or(restrictions.toArray(new HQLRestriction[restrictions.size()])));
			return query.getList();
		}
	}

	private List<HQLRestriction> buildRestrictionListOnParent(RuleQuery query, GazelleCrossValidatorType validator) {
		List<HQLRestriction> restrictions = new ArrayList<HQLRestriction>();
		if (!validator.getValidatorParents().isEmpty()) {
			for (CrossValidatorReference reference : validator.getValidatorParents()) {
				restrictions.add(HQLRestrictions.and(
						query.gazelleCrossValidator().affinityDomain().eqRestriction(reference.getAffinityDomain()),
						query.gazelleCrossValidator().name().eqRestriction(reference.getName())));
			}
		}
		return restrictions;
	}

	public List<Integer> getActiveRuleIdsForValidator(GazelleCrossValidatorType validator) {
		RuleQuery query = new RuleQuery(entityManager);
		query.gazelleCrossValidator().id().eq(validator.getId());
		query.status().eq(RuleStatus.ACTIVE);
		return query.id().getListDistinct();
	}

	public Rule getRuleFromUrlParams(String keyword) {
		RuleQuery query = new RuleQuery(entityManager);
		if (!Identity.instance().isLoggedIn()
				|| (Identity.instance().isLoggedIn() && !(Identity.instance().hasRole("admin_role") || Identity
						.instance().hasRole("tests_editor_role")))) {
			query.status().eq(RuleStatus.ACTIVE);
			query.gazelleCrossValidator().status().eq(Status.AVAILABLE);
		}
		query.keyword().eq(keyword);
		return query.getUniqueResult();
	}

	public Integer countEntries() {
		RuleQuery query = new RuleQuery(entityManager);
		return query.getCount();
	}

	public List<Rule> getRulesForAssertion(String idScheme, String assertionId) {
		RuleQuery query = new RuleQuery(entityManager);
		query.assertions().assertionId().eq(assertionId);
		query.assertions().idScheme().eq(idScheme);
		return query.getListNullIfEmpty();
	}

	public static CoveringEntity toCoveringEntity(Rule rule) {
		String permanentLink = CrossValidatorLinkDataProvider.instance().getLink(rule);
		return new CoveringEntity(rule.getKeyword(), permanentLink);
	}

	@Override
	public Class<Rule> getEntityClass() {
		return Rule.class;
	}

	public void remove(Rule rule) {
		Rule toRemove = find(rule.getId());
		try {
			entityManager.remove(toRemove);
			entityManager.flush();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public Rule saveEntity(Rule rule) {
		rule.onUpdate();
		return super.saveEntity(rule);
	}

	public boolean isKeywordAvailable(String ruleKeyword, Integer ruleId) {
		RuleQuery query = new RuleQuery(entityManager);
		query.keyword().eq(ruleKeyword);
		if (ruleId != null) {
			query.id().neq(ruleId);
		}
		return query.getCount() == 0;
	}

	public List<Rule> getOtherRulesFromValidator(Rule currentRule) {
		RuleQuery query = new RuleQuery(entityManager);
		query.gazelleCrossValidator().id().eq(currentRule.getGazelleCrossValidator().getId());
		if (currentRule.getId() != null) {
			query.id().neq(currentRule.getId());
		}
		query.expression().isNotNull();
		return query.getListDistinct();
	}
}
