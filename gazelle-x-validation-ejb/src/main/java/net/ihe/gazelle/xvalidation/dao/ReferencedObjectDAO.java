package net.ihe.gazelle.xvalidation.dao;

import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObjectQuery;

import javax.persistence.EntityManager;
import java.util.List;

public class ReferencedObjectDAO extends DAO<ReferencedObject> {

	public ReferencedObjectDAO() {
		super();
	}

	public ReferencedObjectDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static ReferencedObjectDAO instanceWithDefaultEntityManager() {
		return new ReferencedObjectDAO();
	}

	public ReferencedObject getReferencedObjectByKeyword(String keyword) {
		ReferencedObjectQuery query = new ReferencedObjectQuery(entityManager);
		query.keyword().eq(keyword);
		return query.getUniqueResult();
	}

	@Override
	public Class<ReferencedObject> getEntityClass() {
		return ReferencedObject.class;
	}

	public List<ReferencedObject> getAllEntities() {
		ReferencedObjectQuery query = new ReferencedObjectQuery(entityManager);
		query.objectType().order(true);
		return query.getList();
	}

	public boolean isKeywordAvailable(String keyword, Integer inputId) {
		ReferencedObjectQuery query = new ReferencedObjectQuery(entityManager);
		query.keyword().eq(keyword);
		if (inputId != null){
			query.id().neq(inputId);
		}
		return query.getCount() == 0;
	}

}
