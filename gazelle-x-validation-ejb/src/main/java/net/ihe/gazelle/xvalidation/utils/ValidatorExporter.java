package net.ihe.gazelle.xvalidation.utils;

import net.ihe.gazelle.common.util.ZipCreation;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ValidatorExporter extends Exporter {

    private static Logger log = LoggerFactory.getLogger(ValidatorExporter.class);

    public ValidatorExporter() {
        super();
    }

    public boolean exportValidator(GazelleCrossValidatorType validator) {
        setOutputStream(new ByteArrayOutputStream());

        List<CrossValidatorReference> parentReferences = validator.getValidatorParents();

        List<GazelleCrossValidatorType> allValidatorsToExport = new ArrayList<>();
        allValidatorsToExport.add(validator);

        listParents(parentReferences, allValidatorsToExport);

        if (!allValidatorsToExport.isEmpty()) {
            exportValidators(allValidatorsToExport);
        }

        return true;
    }

    private void listParents(List<CrossValidatorReference> parentReferences, List<GazelleCrossValidatorType> allValidatorsToExport) {

        for (CrossValidatorReference parentReference : parentReferences) {

            GazelleCrossValidatorType gazelleCrossValidatorType = GazelleCrossValidatorTypeDAO.
                    instanceWithDefaultEntityManager().getValidatorToReference(parentReference);

            if (!allValidatorsToExport.contains(gazelleCrossValidatorType)) {
                allValidatorsToExport.add(gazelleCrossValidatorType);

                List<CrossValidatorReference> validatorReferences = gazelleCrossValidatorType.getValidatorParents();

                listParents(validatorReferences, allValidatorsToExport);
            }
        }

    }

    public String getFilename(GazelleCrossValidatorType validator) {
        StringBuilder filename = new StringBuilder(fixFilename(validator.getName()));
        filename.append('-');
        filename.append(fixFilename(validator.getAffinityDomain()));
        filename.append('-');
        filename.append(validator.getVersion());
        filename.append(".xml");

        return filename.toString();
    }

    public String getZipFolderName(GazelleCrossValidatorType validator) {
        StringBuilder filename = new StringBuilder(fixFilename(validator.getName()));
        filename.append('-');
        filename.append(fixFilename(validator.getAffinityDomain()));
        filename.append('-');
        filename.append(validator.getVersion());
        filename.append(".zip");

        return filename.toString();
    }

    private String fixFilename(final String name) {
        return name.replaceAll("[^\\w]", "_");
    }

    public boolean exportValidators(List<GazelleCrossValidatorType> validators) {
        boolean status = true;
        try {
            List<String> filenames = new ArrayList<String>();
            for (GazelleCrossValidatorType validator : validators) {
                File temp = File.createTempFile("gcv_", getFilename(validator));
                FileOutputStream fos = new FileOutputStream(temp);
                try {
                    toXmlWithStylesheet(fos, validator);
                    filenames.add(temp.getAbsolutePath());
                } catch (JAXBException e) {
                    log.error(e.getMessage(), e);
                }
            }
            byte[] zip = ZipCreation.files2zip(filenames);
            if (zip != null) {
                setOutputStream(new ByteArrayOutputStream());
                getOutputStream().write(zip);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            status = false;
        }
        return status;
    }

    public static void toXmlWithStylesheet(OutputStream out, GazelleCrossValidatorType validator) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(GazelleCrossValidatorType.class);
        Marshaller m = jc.createMarshaller();
        String xvalidatorXsl = PreferenceService.getString("xvalidator_xsl_url");
        String xslDeclaration = "<?xml-stylesheet type=\"text/xsl\" href=\"" + xvalidatorXsl + "\" ?>";
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty("com.sun.xml.bind.xmlHeaders", xslDeclaration);
        m.marshal(validator, out);
    }

}
