package net.ihe.gazelle.xvalidation.refobject.action;

import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("referencedObjectDocumentationManager")
@Scope(ScopeType.PAGE)
public class ReferencedObjectDocumentationManager extends AbstractReferencedObjectBrowser {

	/**
	 *
	 */
	private static final long serialVersionUID = 9151605337298899652L;

	@Override
	public String listValidators() {
		return XValidationPages.DOC_HOME.getLink();
	}

	@Override
	public String listRules() {
		return XValidationPages.DOC_RULES.getLink();
	}

}
