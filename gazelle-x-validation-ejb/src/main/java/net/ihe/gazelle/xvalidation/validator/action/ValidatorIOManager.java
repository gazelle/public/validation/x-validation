package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import net.ihe.gazelle.xvalidation.utils.ValidatorExporter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Name("validatorIOManager")
@Scope(ScopeType.STATELESS)
public class ValidatorIOManager implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 6871482538924002159L;

	private static Logger log = LoggerFactory.getLogger(ValidatorIOManager.class);

	public void exportValidator(final GazelleCrossValidatorType validator) {
		ValidatorExporter exporter = new ValidatorExporter();
		// reload the complete instance of the validator
		GazelleCrossValidatorType validatorFromDB = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
				.getValidatorByUrlParameters(validator.getId(), null,null, null, null);
		if (exporter.exportValidator(validatorFromDB)) {
			try {
				String filename = exporter.getZipFolderName(validatorFromDB);
				exporter.redirectExport("application/zip", filename);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		} else {
			log.error("Unable to export validator: " + validator.getName());
		}
	}

	@SuppressWarnings("unchecked")
	public void exportSelectedValidators(FilterDataModel<GazelleCrossValidatorType> validators) {
		ValidatorExporter exporter = new ValidatorExporter();
		StringBuilder zipname = new StringBuilder("gazelleCrossValidators-");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		zipname.append(sdf.format(new Date()));
		zipname.append(".zip");
		if (exporter.exportValidators((List<GazelleCrossValidatorType>) validators.getAllItems(FacesContext
				.getCurrentInstance()))) {
			try {
				exporter.redirectExport("application/zip", zipname.toString());
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		} else {
			log.error("Unable to export selected validators");
		}
	}

	public static String archiveValidator(final GazelleCrossValidatorType validator) {
		try {
			String baseDir = DeprecatedValidatorVersion.getDirectoryForArchives();
			String archiveFileName = validator.getFilenameForArchive();
			File directory = new File(baseDir);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			File archiveFile = new File(directory, archiveFileName);
			FileOutputStream fos = null;
			try {
				if (archiveFile.exists()) {
					fos = new FileOutputStream(archiveFile);
					GazelleCrossValidatorType validatorToBackup = GazelleCrossValidatorTypeDAO
							.instanceWithDefaultEntityManager().find(validator.getId());
					ValidatorExporter.toXmlWithStylesheet(fos, validatorToBackup);
				}
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
			} finally {
				if (fos != null) {
					fos.close();
				}
			}
			return archiveFile.getName();
		} catch (Exception e) {
			log.error(
					"Cannot back up a version of validator " + validator.getName() + " - "
							+ validator.getAffinityDomain(), e);
			GuiMessage.logMessage(StatusMessage.Severity.WARN, "Be careful, the system did not managed to back up this validator");
			return null;
		}
	}
}
