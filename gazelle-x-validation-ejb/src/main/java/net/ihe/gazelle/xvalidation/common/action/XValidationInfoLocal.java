package net.ihe.gazelle.xvalidation.common.action;


import javax.ejb.Local;
import javax.ws.rs.*;

@Local
@Path("/xvalidation") // base URI will be defined in the web.xml file
public interface XValidationInfoLocal {


    @GET
    @Path("/GetXValidationStatus")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationStatus(@QueryParam("oid") String oid);

    @GET
    @Path("/GetXValidationDate")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationDate(@QueryParam("oid") String oid);

    @GET
    @Path("/GetXValidationPermanentLink")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationPermanentLink(@QueryParam("oid") String oid);

    @GET
    @Path("/GetXValidatorName")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorName(@QueryParam("oid") String oid);

    @GET
    @Path("/GetXValidatorDescription")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorDescription(@QueryParam("oid") String oid);

    @GET
    @Path("/GetXValidatorStatus")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorStatus(@QueryParam("oid") String oid);

    @GET
    @Path("/GetLastXValPermanentLinkWithExternalId")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getLastXValPermanentLinkWithExternalId(@QueryParam("externalId") String externalId, @QueryParam("toolOid") String toolOid);

    @GET
    @Path("/GetLastXValStatusWithExternalId")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getLastXValStatusWithExternalId(@QueryParam("externalId") String externalId, @QueryParam("toolOid") String toolOid);

}

