/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;
import net.ihe.gazelle.xvalidation.utils.AbstractPieChart;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.primefaces.model.chart.PieChartModel;

import java.util.List;

/**
 * <b>Class Description : </b>CrossValidatorStatistics<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 17/02/16
 * @class CrossValidatorStatistics
 * @package net.ihe.gazelle.xvalidation.validator.action
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@Name("ruleStatistics")
@Scope(ScopeType.STATELESS)
public class RuleStatistics extends AbstractPieChart {

    public PieChartModel getPieChartUnitTest(Rule rule) {
        PieChartModel pieChartModel = createPieChart("Status of unit tests", "e");
        if (rule != null) {
            RuleUnitTestQuery query = new RuleUnitTestQuery();
            query.testedRule().id().eq(rule.getId());
            List<Object[]> stats = query.lastResult().getStatistics();
            formatUnitTestStat(pieChartModel, stats);
        }
        return pieChartModel;
    }

}
