package net.ihe.gazelle.xvalidation.refobject.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.ReferencedObjectDAO;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("referencedObjectDisplayManager")
@Scope(ScopeType.PAGE)
public class ReferencedObjectDisplayManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5073316679833310503L;
	private ReferencedObject reference;

	@Create
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		reference = ReferencedObjectDAO.instanceWithDefaultEntityManager().getReferencedObjectByKeyword(
				params.get("keyword"));
	}

	public List<GazelleCrossValidatorType> getCrossValidators() {
		return GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorForReferencedObject(
				reference);
	}

	public ReferencedObject getReference() {
		return reference;
	}

	public void setReference(ReferencedObject reference) {
		this.reference = reference;
	}

}
