package net.ihe.gazelle.xvalidation.rule.action;

import junit.framework.AssertionFailedError;
import net.ihe.gazelle.assertion.AssertionWsClient;
import net.ihe.gazelle.assertion.ws.data.WsAssertion;
import net.ihe.gazelle.assertion.ws.data.WsIdScheme;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInputQuery;
import net.ihe.gazelle.xvalidation.dao.*;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("ruleEditorManager")
@Scope(ScopeType.PAGE)
public class RuleEditorManager implements Serializable, QueryModifier {

    /**
     *
     */
    private static final long serialVersionUID = 8806104596352787270L;

    private static Logger log = LoggerFactory.getLogger(RuleEditorManager.class);
    private static final String RIGHT = "RIGHT";
    private static final String LEFT = "LEFT";
    private static final String ONE = "member1";
    private static final String TWO = "member2";
    private static final String MAIN_EXPRESSION = "mainEx";
    private static final String BOARD_NAME = "board";
    private Rule newRule;
    private boolean editMode = false;
    private static final List<SelectItem> SEVERITIES;
    private List<AbstractExpression> board;
    private transient Integer validatorId;
    private List<String> keywords;
    private Rule ruleToCopy;
    private transient GazelleCrossValidatorType validator;
    private List<WsAssertion> availableAssertions;
    private String searchedIdScheme = null;
    private List<String> availableSchemes;
    private Integer exToMoveFakeId;
    private static List<SelectItem> COMPARATORS;
    private static List<SelectItem> DATE_COMPARATORS;

    private FilterDataModel<ValidatorInput> ruleInputs;
    private Filter<ValidatorInput> filter;

    private List<RuleUnitTest> ruleUnitTests;

    static {
        SEVERITIES = new ArrayList<SelectItem>();
        for (Level level : Level.values()) {
            SEVERITIES.add(new SelectItem(level, level.value()));
        }
        COMPARATORS = new ArrayList<SelectItem>();
        for (MathComparatorType type : MathComparatorType.values()) {
            COMPARATORS.add(new SelectItem(type, type.value()));
        }
        DATE_COMPARATORS = new ArrayList<SelectItem>();
        for (DateOperatorType type : DateOperatorType.values()) {
            DATE_COMPARATORS.add(new SelectItem(type, type.value()));
        }
    }

    @Create
    public void initializeRuleEdition() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        validator = null;
        newRule = null;
        if (params.containsKey("validatorId")) {
            validatorId = Integer.parseInt(params.get("validatorId"));
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().find(validatorId);
            if (validator == null) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The given validatorId does not reference an existing validator");
            }
        }
        if (params.containsKey("id")) {
            Integer ruleId = Integer.parseInt(params.get("id"));
            newRule = RuleDAO.instanceWithDefaultEntityManager().find(ruleId);
            if (newRule == null) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The given id does not reference an existing rule");
            }
            validator = newRule.getGazelleCrossValidator();
        } else if (validator != null) {
            // We are creating a new rule
            newRule = new Rule(validator);
        }
        if (newRule != null) {
            board = new ArrayList<AbstractExpression>();
            keywords = ValidatorInputDAO.instanceWithDefaultEntityManager().getInputKeywordsForValidator(
                    newRule.getGazelleCrossValidator());
        }
        listAvailableSchemes();
    }

    public List<SelectItem> getSeverities() {
        return SEVERITIES;
    }

    public String back() {
        if (validator.getId() != null) {
            return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
        } else {
            return XValidationPages.ADMIN_RULES.getLink();
        }
    }

    public void markRuleAsActive() {
        updateStatus(RuleStatus.ACTIVE);
    }

    public void markRuleAsInactive() {
        updateStatus(RuleStatus.INACTIVE);
    }

    public void markRuleAsDeprecated() {
        updateStatus(RuleStatus.DEPRECATED);
    }

    private void updateStatus(RuleStatus newStatus) {
        newRule.setStatus(newStatus);
        if (newStatus.equals(RuleStatus.INACTIVE)) {
            newRule.setVersion(newRule.getVersion() + GazelleCrossValidatorType.DEV_SUFFIX);
        }
        saveRule();
    }

    public String duplicateRule() {
        RuleDAO dao = RuleDAO.instanceWithDefaultEntityManager();
        Rule copy = new Rule(dao.find(newRule.getId()));
        copy = dao.saveEntity(copy);
        return createANewRule() + "&id=" + copy.getId();
    }

    /**
     * instanciate a new rule for the same validator
     *
     * @return
     */
    public String createANewRule() {
        return XValidationPages.ADMIN_EDIT_RULE.getSeamLink() + "?validatorId=" + validator.getId();
    }

    public void saveRule() {
        if (((newRule.getExpression() == null) || (newRule.getExpression().getExpression() == null)) || (!newRule.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX) && newRule.getStatus().equals(RuleStatus.INACTIVE))) {
            newRule.setStatus(RuleStatus.INACTIVE);
            newRule.setVersion(newRule.getVersion() + GazelleCrossValidatorType.DEV_SUFFIX);
        }
        newRule = RuleDAO.instanceWithDefaultEntityManager().saveEntity(newRule);
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Rule has been saved");
    }

    public void editExpression() {
        editMode = true;
        if (newRule.getExpression() != null && newRule.getExpression().getExpression() != null) {
            board.add(newRule.getExpression().getExpression());
        }
    }

    public void closeExpressionPanel() {
        editMode = false;
    }

    public void deleteExpression(int expressionId) {
        AbstractExpression element = null;
        for (AbstractExpression boardElement : board) {
            if (boardElement.getFakeId() == expressionId) {
                element = boardElement;
            }
        }
        if (element == null) {
            element = getExpressionWithFakeid(expressionId, true);
        }
        board.remove(element);
    }

    private AbstractExpression getExpressionWithFakeid(int fakeId, boolean remove) {
        return getExpressionFromList(fakeId, board, remove);
    }

    private AbstractExpression getExpressionFromList(int fakeId, List<AbstractExpression> expressionSet, boolean remove) {
        AbstractExpression result = null;
        boolean toRemove = false;
        if (expressionSet != null && !expressionSet.isEmpty()) {
            for (AbstractExpression expression : expressionSet) {
                if (expression.getFakeId() == fakeId) {
                    result = expression;
                    toRemove = remove;
                } else if (expression instanceof LogicalOperation) {
                    result = getExpressionFromList(fakeId, ((LogicalOperation) expression).getExpressionSet(), remove);
                } else if ((expression instanceof ForEach) || (expression instanceof Not)) {
                    result = getExpressionRecursive(fakeId, expression, remove);
                } else {
                    continue;
                }
                if (result != null) {
                    break;
                }
            }
        }
        if ((result != null) && toRemove) {
            expressionSet.remove(result);
        }
        return result;
    }

    private AbstractExpression getExpressionRecursive(int fakeId, AbstractExpression expression, boolean remove) {
        AbstractExpression result = null;
        AbstractExpression expressionToLoopOn = null;
        if (expression instanceof Not) {
            expressionToLoopOn = ((Not) expression).getExpression();
        } else if (expression instanceof ForEach) {
            expressionToLoopOn = ((ForEach) expression).getExpressionSet();
        }
        if (expressionToLoopOn == null) {
            return null;
        } else if (expressionToLoopOn.getFakeId() == fakeId) {
            result = expressionToLoopOn;
            if (remove) {
                if (expression instanceof Not) {
                    ((Not) expression).setExpression(null);
                } else if (expression instanceof ForEach) {
                    ((ForEach) expression).setExpressionSet(null);
                }
            }
            return result;
        } else if (expressionToLoopOn instanceof LogicalOperation) {
            return getExpressionFromList(fakeId, ((LogicalOperation) expressionToLoopOn).getExpressionSet(), remove);
        } else if ((expressionToLoopOn instanceof ForEach) || (expressionToLoopOn instanceof Not)) {
            return getExpressionRecursive(fakeId, expression, remove);
        }
        return result;
    }

    public void addToBoard(AbstractExpression expression, Integer targetId) {
        if (targetId == 0) {
            board.add(expression);
        } else {
            AbstractExpression targetExpression = getExpressionWithFakeid(targetId, false);
            if (targetExpression != null) {
                if (targetExpression instanceof ForEach) {
                    ((ForEach) targetExpression).setExpressionSet(expression);
                } else if (targetExpression instanceof LogicalOperation) {
                    ((LogicalOperation) targetExpression).addExpressionSet(expression);
                } else if (targetExpression instanceof Not) {
                    ((Not) targetExpression).setExpression(expression);
                }
            } else {
                log.error("[target expression] no expression found with fakeId = " + targetId);
            }
        }
    }

    public void addToMathComparison(MathMember expression, Integer targetId, String side) {
        AbstractExpression targetExpression = getExpressionWithFakeid(targetId, false);
        if (targetExpression != null) {
            if (targetExpression instanceof MathComparison) {
                if (side.equals("left")) {
                    ((MathComparison) targetExpression).setLeftMember(expression);
                } else {
                    ((MathComparison) targetExpression).setRightMember(expression);
                }
            }
        }
    }


    public List<Rule> listExistingRules() {
        return RuleDAO.instanceWithDefaultEntityManager().getOtherRulesFromValidator(newRule);
    }

    public void createCopy() {
        if (ruleToCopy != null) {
            AbstractExpression newExpression = ruleToCopy.copyExpression();
            if (newExpression != null) {
                board.add(newExpression);
            } else {
                log.error("The expression of the selected rule cannot be duplicated");
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The expression of the selected rule cannot be duplicated");
            }
        }
    }

    public ValidatorConfiguration getValidatorConfiguration() {
        return newRule.getValidatorConfiguration();
    }

    public FilterDataModel<ValidatorInput> getRuleInputs() {
        return new FilterDataModel<ValidatorInput>(getFilter()) {
            @Override
            protected Object getId(ValidatorInput validatorInput) {
                return validatorInput.getId();
            }
        };
    }

    public Filter<ValidatorInput> getFilter() {
        if (filter == null) {
            this.filter = new Filter(this.getHqlCriterions());
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        ValidatorInputQuery query = new ValidatorInputQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("objectType", query.referencedObject().objectType());
        criterion.addPath("keyword", query.referencedObject().keyword());
        criterion.addPath("description", query.referencedObject().description());
        criterion.addQueryModifier(this);
        return criterion;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }

    @Override
    public void modifyQuery(HQLQueryBuilder hqlQueryBuilder, Map map) {
        ValidatorInputQuery query = new ValidatorInputQuery();
        RuleUnitTestQuery unitTestQuery = new RuleUnitTestQuery();
        if (newRule.getGazelleCrossValidator() != null) {
            hqlQueryBuilder.addRestriction(query.gazelleCrossValidator().id().eqRestriction(newRule.getGazelleCrossValidator().getId()));
            //hqlQueryBuilder.addRestriction(unitTestQuery.testedRule().id().eqRestriction(newRule.getId()));
        }
    }

    public void createAnd(Integer target) {
        addToBoard(new LogicalOperation(LogicalOperatorType.AND), target);
    }

    public void createOr(Integer target) {
        addToBoard(new LogicalOperation(LogicalOperatorType.OR), target);
    }

    public void createXor(Integer target) {
        addToBoard(new LogicalOperation(LogicalOperatorType.XOR), target);
    }

    public void createDateComparison(Integer target){
        addToBoard(new DateComparison(DateOperatorType.EQUAL), target);
    }

    public void createForEach(Integer target) {
        addToBoard(new ForEach(), target);
    }

    public void createIn(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.IN), target);
    }

    public void createNot(Integer target) {
        addToBoard(new Not(), target);
    }

    public void createContain(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.CONTAIN), target);
    }

    public void createEq(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.EQUAL), target);
    }

    public void createSizeEq(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.SIZEEQUAL), target);
    }

    public void createSizeGreater(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.SIZEGREATEROREQUAL), target);
    }

    public void createSizeLess(Integer target) {
        addToBoard(new BasicOperation(BasicOperatorType.SIZELESSOREQUAL), target);
    }

    public void createPresent(Integer target) {
        addToBoard(new Present(), target);
    }

    public void createTrue(Integer target) {
        addToBoard(new True(), target);
    }

    public void createInValueSet(Integer target) {
        addToBoard(new InValueSet(), target);
    }

    public void createMathComparison(Integer target) {
        addToBoard(new MathComparison(), target);
    }

    public void createAddition(Integer target, String side) {
        addMathMember(new MathOperation(MathOperatorType.PLUS), target, side);
    }

    public void createSoustraction(Integer target, String side) {
        addMathMember(new MathOperation(MathOperatorType.MINUS), target, side);
    }

    public void createMultiplication(Integer target, String side) {
        addMathMember(new MathOperation(MathOperatorType.TIMES), target, side);
    }

    public void createDivision(Integer target, String side) {
        addMathMember(new MathOperation(MathOperatorType.DIVIDE), target, side);
    }

    public void createSingle(Integer target, String side) {
        addMathMember(new SingleMember(), target, side);
    }

    public void createConstant(Integer target, String side) {
        addMathMember(new ConstantMember(), target, side);
    }

    public void createPower(Integer target, String side) {
        addMathMember(new MathOperation(MathOperatorType.POWER), target, side);
    }

    public void deleteMathMember(Integer target) {
        findMathMember(target, true);
    }

    private void addMathMember(MathMember member, Integer target, String side) {
        // first check if we are looking for parent mathOperation or mathMember
        if (side.equals(RIGHT)) {
            MathComparison comparison = findMathComparison(target);
            if (comparison != null) {
                comparison.setRightMember(member);
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "System was not able to find parent with id " + target);
            }
        } else if (side.equals(LEFT)) {
            MathComparison comparison = findMathComparison(target);
            if (comparison != null) {
                comparison.setLeftMember(member);
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "System was not able to find parent with id " + target);
            }
        } else if (side.equals(ONE)) {
            MathOperation operation = (MathOperation) findMathMember(target, false);
            if (operation != null) {
                operation.setMember1(member);
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "System was not able to find parent with id " + target);
            }
        } else { // last possible value is TWO
            MathOperation operation = (MathOperation) findMathMember(target, false);
            if (operation != null) {
                operation.setMember2(member);
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "System was not able to find parent with id " + target);
            }
        }
    }

    private MathMember findMathMember(Integer target, boolean remove) {
        MathOperation found = null;
        for (AbstractExpression ex : board) {
            if (ex instanceof MathComparison) {
                MathComparison comparison = (MathComparison) ex;
                if (comparison.getRightMember() != null) {
                    if (comparison.getRightMember().getFakeId() == target) {
                        MathMember member = comparison.getRightMember();
                        if (remove) {
                            comparison.setRightMember(null);
                        }
                        return member;
                    } else if (comparison.getRightMember() instanceof MathOperation) {
                        found = lookInMathOperation((MathOperation) comparison.getRightMember(), target, remove);
                        if (found != null) {
                            break;
                        }
                    }
                }
                if (comparison.getLeftMember() != null) {
                    if (comparison.getLeftMember().getFakeId() == target) {
                        MathMember member = comparison.getLeftMember();
                        if (remove) {
                            comparison.setLeftMember(null);
                        }
                        return member;
                    } else if (comparison.getLeftMember() instanceof MathOperation) {
                        found = lookInMathOperation((MathOperation) comparison.getLeftMember(), target, remove);
                        if (found != null) {
                            break;
                        }
                    }
                }
            }
        }
        return found;
    }

    private MathOperation lookInMathOperation(MathOperation operation, Integer target, boolean remove) {
        if (operation.getFakeId() == target) {
            return operation;
        } else if (operation.getMember1() != null && operation.getMember1() instanceof MathOperation) {
            MathOperation member1 = lookInMathOperation((MathOperation) operation.getMember1(), target, false);
            if (member1 != null && remove) {
                operation.setMember1(null);
            }
            if (member1 != null) {
                return member1;
            } else if (operation.getMember2() != null && operation.getMember2() instanceof MathOperation) {
                MathOperation member2 = lookInMathOperation((MathOperation) operation.getMember2(), target, false);
                if (member2 != null && remove) {
                    operation.setMember2(null);
                }
                if (member2 != null) {
                    return member2;
                }
            }
        }
        return null;
    }

    private MathComparison findMathComparison(Integer target) {
        AbstractExpression expression = getExpressionWithFakeid(target, false);
        if (expression != null && expression instanceof MathComparison) {
            return (MathComparison) expression;
        } else {
            return null;
        }
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public void setMainExpression(Integer fakeId) {
        AbstractExpression expression = getExpressionWithFakeid(fakeId, true);
        try {
            expression.testCorrectness();
            newRule.getExpression().setExpression(expression);
            newRule.setAppliesTo(expression.appliesTo());
            closeExpressionEdition();
        } catch (AssertionFailedError e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage(), e);
        }
    }

    private void closeExpressionEdition() {
        this.editMode = false;
        newRule.setTextualExpression(null);
        clearBoard();
        saveRule();
    }

    public void clearBoard() {
        board = new ArrayList<AbstractExpression>();
    }

    public void validateRuleKeyword(FacesContext context, UIComponent component, Object value)
            throws ValidatorException {
        if (value instanceof String) {
            String ruleKeyword = (String) value;
            Integer ruleId = newRule.getId();
            if (!RuleDAO.instanceWithDefaultEntityManager().isKeywordAvailable(ruleKeyword, ruleId)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Rule keyword shall be unique accross the application and this one is already used", null));
            }
        }
    }

    public void listAvailableSchemes() {
        AssertionWsClient client = new AssertionWsClient(getAssertionManagerApiUrl());
        List<WsIdScheme> schemes = client.getIdSchemes();
        availableSchemes = new ArrayList<String>();
        if (schemes != null && !schemes.isEmpty()) {
            for (WsIdScheme scheme : schemes) {
                availableSchemes.add(scheme.getIdScheme());
            }
        }
    }

    private String getAssertionManagerApiUrl() {
        String url;
        if (!validator.getAssertionManagerUrl().endsWith("/")) {
            url = validator.getAssertionManagerUrl().concat("/");
        } else {
            url = validator.getAssertionManagerUrl();
        }
        return url + "rest/";
    }

    public void listAssertions() {
        AssertionWsClient client = new AssertionWsClient(getAssertionManagerApiUrl());
        availableAssertions = client.getAssertionsForScheme(searchedIdScheme);
    }

    public void addAssertion(WsAssertion newAssertion) {
        if (newAssertion != null) {
            Assertion assertion = new Assertion(newAssertion.getIdScheme(), newAssertion.getAssertionId(), newAssertion.getDescription());
            if (!newRule.getAssertions().contains(assertion)) {
                assertion.addRule(newRule);
                newRule.addAssertion(assertion);
                saveRule();
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.WARN, "This assertion is already linked to that rule");
            }
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The selected assertion is empty");
        }
    }

    public boolean canChangeRuleStatus() {
        if (newRule.getId() == null) {
            return false;
        } else if (RuleStatus.DEPRECATED.equals(newRule.getStatus())) {
            return false;
        } else {
            return (newRule.getTextualExpression() != null && !newRule.getTextualExpression().isEmpty());
        }
    }

    public void cancelExpressionEdition() {
        editMode = false;
        clearBoard();
        newRule = RuleDAO.instanceWithDefaultEntityManager().find(newRule.getId());
    }

    public void removeAssertion(Assertion assertion) {
        Assertion assertionToRemove = EntityManagerService.provideEntityManager().find(Assertion.class, assertion.getId());
        newRule.removeAssertion(assertionToRemove);
        assertionToRemove.removeRule(newRule);
        saveRule();
    }

    public List<String> listIdSchemes() {
        return AssertionDAO.instanceWithDefaultEntityManager().getIdSchemes();
    }


    public GazelleListDataModel<RuleUnitTest> getUnitTests() {
        this.ruleUnitTests = RuleUnitTestDAO.instanceWithDefaultEntityManager().getUnitTestsForRule(newRule);
        if (this.ruleUnitTests != null) {
            return new GazelleListDataModel<>(this.ruleUnitTests);
        } else {
            return null;
        }
    }

    public List<AbstractExpression> getBoard() {
        return board;
    }

    public void setBoard(List<AbstractExpression> board) {
        this.board = board;
    }

    public Rule getNewRule() {
        return newRule;
    }

    public void setNewRule(Rule newRule) {
        this.newRule = newRule;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getMainExpression() {
        return MAIN_EXPRESSION;
    }

    public String getBoardName() {
        return BOARD_NAME;
    }

    public Rule getRuleToCopy() {
        return ruleToCopy;
    }

    public void setRuleToCopy(Rule ruleToCopy) {
        this.ruleToCopy = ruleToCopy;
    }

    public String getSearchedIdScheme() {
        return searchedIdScheme;
    }

    public void setSearchedIdScheme(String searchedIdScheme) {
        this.searchedIdScheme = searchedIdScheme;
    }

    public List<WsAssertion> getAvailableAssertions() {
        return availableAssertions;
    }

    public List<String> getAvailableSchemes() {
        return availableSchemes;
    }

    public boolean isLinked(WsAssertion wsassertion) {
        if (wsassertion != null) {
            Assertion assertion = new Assertion(wsassertion.getIdScheme(), wsassertion.getAssertionId(), null);
            return newRule.getAssertions().contains(assertion);
        } else {
            return false;
        }
    }

    public void removeAssertion(WsAssertion wsassertion) {
        Assertion assertion = AssertionDAO.instanceWithDefaultEntityManager().getAssertionByIdAndIdScheme(wsassertion.getIdScheme(), wsassertion
                .getAssertionId());
        if (assertion != null) {
            removeAssertion(assertion);
        }
    }

    public Integer getExToMoveFakeId() {
        return exToMoveFakeId;
    }

    public void setExToMoveFakeId(Integer exToMoveFakeId) {
        this.exToMoveFakeId = exToMoveFakeId;
    }

    public void moveTo(Integer receivedFakeId) {
        if (exToMoveFakeId != null && receivedFakeId != null) {
            AbstractExpression receiver = getExpressionWithFakeid(receivedFakeId, false);
            if (receiver != null) {
                AbstractExpression exToMove = getExpressionWithFakeid(exToMoveFakeId, true);
                if (receiver instanceof Not) {
                    ((Not) receiver).setExpression(exToMove);
                } else if (receiver instanceof ForEach) {
                    ((ForEach) receiver).setExpressionSet(exToMove);
                } else if (receiver instanceof LogicalOperation) {
                    ((LogicalOperation) receiver).addExpressionSet(exToMove);
                }
            }
        }
        exToMoveFakeId = null;
    }

    public List<SelectItem> listComparators() {
        return COMPARATORS;
    }

    public List<SelectItem> listDateComparators() {
        return DATE_COMPARATORS;
    }

    public List<DateFormatType> listDateFormats() {
        return (new DateFormatTypeDAO()).getAllEntities();
    }

}