package net.ihe.gazelle.xvalidation.validator.action;

import java.util.Map;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorTypeQuery;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;

public abstract class AbstractValidatorBrowser implements QueryModifier<GazelleCrossValidatorType> {

	/**
	 *
	 */
	private static final long serialVersionUID = 3776851040218894588L;

	private String filteredParent;

	private Filter<GazelleCrossValidatorType> filter;

	private boolean emptyValidatorList = true;

	public Filter<GazelleCrossValidatorType> getFilter() {
		if (filter == null) {
			filter = new Filter<GazelleCrossValidatorType>(buildHQLCriterions());
		}
		return filter;
	}

	public FilterDataModel<GazelleCrossValidatorType> getGazelleCrossValidators() {
		return new FilterDataModel<GazelleCrossValidatorType>(getFilter()){
                    @Override
        protected Object getId(GazelleCrossValidatorType t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
	}

	private HQLCriterionsForFilter<GazelleCrossValidatorType> buildHQLCriterions() {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		HQLCriterionsForFilter<GazelleCrossValidatorType> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("objectType", query.validatedObjects().referencedObject());
		addSpecificHQLCriterions(criteria, query);
		criteria.addQueryModifier(this);
		return criteria;
	}

	public void resetFilter() {
		getFilter().clear();
	}

	protected abstract void addSpecificHQLCriterions(HQLCriterionsForFilter<GazelleCrossValidatorType> criterions,
			GazelleCrossValidatorTypeQuery query);

	public boolean isEmptyValidatorList() {
		if (emptyValidatorList) {
			emptyValidatorList = (GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().countEntries() == 0);
		}
		return emptyValidatorList;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<GazelleCrossValidatorType> queryBuilder,
			Map<String, Object> filterValuesApplied) {
		if ((filteredParent != null) && !filteredParent.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("parent", filteredParent,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
	}

	public String getFilteredParent() {
		return filteredParent;
	}

	public void setFilteredParent(String filteredParent) {
		this.filteredParent = filteredParent;
	}

	public abstract String listRules();

	public abstract String listReferencedObjects();
}
