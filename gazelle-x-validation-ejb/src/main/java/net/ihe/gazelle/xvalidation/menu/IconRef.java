package net.ihe.gazelle.xvalidation.menu;

public final class IconRef {

	private IconRef() {

	}

	public static final String DEFAULT_ICON = "fa-play";
	public static final String DOC_ICON = "fa-book";
}
