package net.ihe.gazelle.xvalidation.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.xvalidation.core.engine.ValidationEngine;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;

public class ValidationEngineWithDB extends ValidationEngine {

	public ValidationEngineWithDB(GazelleCrossValidatorType validator, Map<String, Object> files)
			throws GazelleXValidationException, IOException {
		super(validator, files);
	}
	
	@Override
	protected List<Rule> getAdditionalRulesToExecute(){
		return RuleDAO.instanceWithDefaultEntityManager().getRulesFromParents(getValidator());
	}

}
