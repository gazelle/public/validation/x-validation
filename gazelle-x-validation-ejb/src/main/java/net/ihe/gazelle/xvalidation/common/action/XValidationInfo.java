package net.ihe.gazelle.xvalidation.common.action;

import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.text.SimpleDateFormat;
import java.util.Date;

@Stateless
@Name("XValidationInfo")
public class XValidationInfo implements XValidationInfoLocal {


    public static final String OID = "oid";
    public static final String EXTERNAL_ID = "externalId";
    public static final String TOOL_OID = "toolOid";
    public static final String TEXT_PLAIN = "text/plain";


    @Override
    @GET
    @Path("/GetXValidationDate")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationDate(@QueryParam(XValidationInfo.OID)
                                         final String oid) {
        Lifecycle.beginCall();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = CrossValidationLogDAO.instanceWithDefaultEntityManager().getDateByOid(oid);
        String result = formatter.format(date);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetXValidationPermanentLink")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationPermanentLink(@QueryParam(XValidationInfo.OID)
                                                  final String oid) {
        Lifecycle.beginCall();
        String result = CrossValidationLogDAO.instanceWithDefaultEntityManager().getPermanentLinkByOid(oid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetXValidationStatus")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidationStatus(@QueryParam(XValidationInfo.OID)
                                      final String oid) {
        Lifecycle.beginCall();
        String result = CrossValidationLogDAO.instanceWithDefaultEntityManager().getStatusByOid(oid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetXValidatorName")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorName(@QueryParam(XValidationInfo.OID)
                                       final String oid) {
        Lifecycle.beginCall();
        String result = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorNameByOid(oid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetXValidatorDescription")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorDescription(@QueryParam(XValidationInfo.OID)
                                    final String oid) {
        Lifecycle.beginCall();
        String result = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorDescriptionByOid(oid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetXValidatorStatus")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getXValidatorStatus(@QueryParam(XValidationInfo.OID)
                                           final String oid) {
        Lifecycle.beginCall();
        String result = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorStatusByOid(oid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetLastXValPermanentLinkWithExternalId")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getLastXValPermanentLinkWithExternalId(@QueryParam(XValidationInfo.EXTERNAL_ID)
                                      final String externalId, @QueryParam(XValidationInfo.TOOL_OID) final String toolOid) {
        Lifecycle.beginCall();
        String result = CrossValidationLogDAO.instanceWithDefaultEntityManager().getLastXValPermanentLinkFromExternalId(externalId, toolOid);
        Lifecycle.endCall();
        return result;
    }

    @Override
    @GET
    @Path("/GetLastXValStatusWithExternalId")
    @Produces(XValidationInfo.TEXT_PLAIN)
    public String getLastXValStatusWithExternalId(@QueryParam(XValidationInfo.EXTERNAL_ID)
                                                         final String externalId, @QueryParam(XValidationInfo.TOOL_OID) final String toolOid) {
        Lifecycle.beginCall();
        String result = CrossValidationLogDAO.instanceWithDefaultEntityManager().getLastXValStatusFromExternalId(externalId, toolOid);
        Lifecycle.endCall();
        return result;
    }


}
