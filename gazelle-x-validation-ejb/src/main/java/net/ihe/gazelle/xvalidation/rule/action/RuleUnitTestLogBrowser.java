package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathEntityCriterion;
import net.ihe.gazelle.validator.ut.action.UnitTestLogBrowser;
import net.ihe.gazelle.validator.ut.action.UnitTestManager;
import net.ihe.gazelle.validator.ut.model.UnitTestLog;
import net.ihe.gazelle.validator.ut.model.UnitTestLogQuery;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;

import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("ruleUnitTestLogBrowser")
@Scope(ScopeType.PAGE)
public class RuleUnitTestLogBrowser extends UnitTestLogBrowser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4644632965342106571L;

	@Override
	protected void addPathToCriteria(HQLCriterionsForFilter<UnitTestLog> criteria) {
		criteria.addCriterion(new HQLSafePathEntityCriterion(Rule.class, "rule", "unitTest.testedRule"));
		criteria.addCriterion(new HQLSafePathEntityCriterion(GazelleCrossValidatorType.class, "validator", "unitTest.testedRule.gazelleCrossValidator"));
	}

	public String getUnitTestReport(UnitTestLog selectedLog){
		return "/xvalidation/testing/ruleUnitTestLogDetails.seam?logId="+selectedLog.getId();
	}

	public String executeAgain(){
		RuleUnitTestManager unitTestManager = (RuleUnitTestManager) Component.getInstance("ruleUnitTestManager");
		unitTestManager.execute((RuleUnitTest) getSelectedLog().getUnitTest());
		UnitTestLogQuery query = new UnitTestLogQuery();
		query.unitTest().eq(getSelectedLog().getUnitTest());
		query.runDate().order(false);
		return getUnitTestReport(query.getUniqueResult());
	}
}
