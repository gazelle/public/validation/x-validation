package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInputQuery;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Name("ruleDisplayManager")
@Scope(ScopeType.PAGE)
public class RuleDisplayManager implements Serializable, QueryModifier {

	/**
	 *
	 */
	private static final long serialVersionUID = -3918421002283639791L;
	private Rule rule;

	private FilterDataModel<ValidatorInput> ruleInputs;
	private Filter<ValidatorInput> filter;


	@Create
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		rule = RuleDAO.instanceWithDefaultEntityManager().getRuleFromUrlParams(params.get("keyword"));
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public String editRule() {
		return XValidationPages.ADMIN_EDIT_RULE.getSeamLink() + "?id=" + rule.getId();
	}

	public FilterDataModel<ValidatorInput> getRuleInputs() {
		return new FilterDataModel<ValidatorInput>(getFilter()) {
			@Override
			protected Object getId(ValidatorInput validatorInput) {
				return validatorInput.getId();
			}
		};
	}

	public Filter<ValidatorInput> getFilter() {
		if (filter == null) {
			this.filter = new Filter(this.getHqlCriterions());
		}
		return this.filter;
	}

	private HQLCriterionsForFilter getHqlCriterions() {
		ValidatorInputQuery query = new ValidatorInputQuery();
		HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
		criterion.addPath("objectType", query.referencedObject().objectType());
		criterion.addPath("keyword", query.referencedObject().keyword());
		criterion.addPath("description", query.referencedObject().description());
		criterion.addQueryModifier(this);
		return criterion;
	}

	public void reset() {
		if (this.filter != null) {
			this.filter.clear();
		}
	}

	@Override
	public void modifyQuery(HQLQueryBuilder hqlQueryBuilder, Map map) {
		ValidatorInputQuery query = new ValidatorInputQuery();
		if (rule.getGazelleCrossValidator() != null) {
			hqlQueryBuilder.addRestriction(query.gazelleCrossValidator().id().eqRestriction(rule.getGazelleCrossValidator().getId()));
		}
	}
}
