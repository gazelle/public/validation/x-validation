package net.ihe.gazelle.xvalidation.refobject.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.ReferencedObjectDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("referencedObjectEditorManager")
@Scope(ScopeType.PAGE)
public class ReferencedObjectEditorManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 400032611787263757L;

	private static final List<SelectItem> AVAILABLE_KINDS;
	private ReferencedObject refObject;
	private boolean duplicate;
	
	static{
		AVAILABLE_KINDS = new ArrayList<SelectItem>();
		for (Kind kind : Kind.values()) {
			AVAILABLE_KINDS.add(new SelectItem(kind, kind.getValue()));
		}
	}

	@Create
	public void initialize() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String keyword = params.get("keyword");
		duplicate = params.containsKey("duplicate");
		if (keyword != null) {
			refObject = ReferencedObjectDAO.instanceWithDefaultEntityManager().getReferencedObjectByKeyword(keyword);
			if (refObject == null) {
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, keyword + "does not match an existing referenced object");
			} else if (duplicate) {
				refObject = new ReferencedObject(refObject);
			}
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "keyword is a required parameter");
		}
	}

	public ReferencedObject getRefObject() {
		return refObject;
	}

	public void setRefObject(ReferencedObject refObject) {
		this.refObject = refObject;
	}

	public void save() {
		if (refObject != null) {
			refObject = ReferencedObjectDAO.instanceWithDefaultEntityManager().saveEntity(refObject);
			GuiMessage.logMessage(StatusMessage.Severity.INFO, "Referenced object has been saved");
		}
	}

	public List<GazelleCrossValidatorType> getCrossValidators() {
		return GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorForReferencedObject(
				refObject);
	}
	
	public void validateKeyword(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		if (value instanceof String) {
			String keyword = (String) value;
			if (!ReferencedObjectDAO.instanceWithDefaultEntityManager().isKeywordAvailable(keyword, refObject.getId())) {
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Keyword shall be unique accross the application and this one is already used",
						null));
			}
		}
	}
	
	public List<SelectItem> getAvailableKinds(){
		return AVAILABLE_KINDS;
	}

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}
}
