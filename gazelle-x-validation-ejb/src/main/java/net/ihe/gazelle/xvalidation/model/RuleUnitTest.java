package net.ihe.gazelle.xvalidation.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import net.ihe.gazelle.validator.ut.model.UnitTest;
import net.ihe.gazelle.xvalidation.core.model.Rule;

/**
 * This entity stores the information about the unit tests of rules
 * @author aberge
 *
 */
@Entity
@DiscriminatorValue("XVAL_RULE_UT")
public class RuleUnitTest extends UnitTest implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 904725100611305807L;

	@ManyToOne(targetEntity=Rule.class)
	@JoinColumn(name="tested_rule_id")
	private Rule testedRule;

	@ElementCollection(targetClass = String.class)
	@CollectionTable(name = "inputs_with_file_keywords_ut" , joinColumns = @JoinColumn(name = "tested_rule_id"))
	@Column(name = "inputs_with_file_keywords")
	private List<String> inputsWithFileKeyword;


	public RuleUnitTest(){
		super();
	}

	public RuleUnitTest(Rule rule){
		super();
		this.testedRule = rule;
		setKeyword(TEST_PREFIX + rule.getKeyword() + SEPARATOR);
	}

	public RuleUnitTest(RuleUnitTest test){
		super(test);
		this.testedRule = test.getTestedRule();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	public Rule getTestedRule() {
		return testedRule;
	}

	public void setTestedRule(Rule testedRule) {
		this.testedRule = testedRule;
	}

	public List<String> getInputsWithFileKeyword() {
		return inputsWithFileKeyword;
	}

	public void setInputsWithFileKeyword(List<String> inputsWithFileKeyword) {
		this.inputsWithFileKeyword = inputsWithFileKeyword;
	}

	public void addInputWithFileKeyword(String inputWithFileKeyword) {
		if (this.inputsWithFileKeyword != null) {
			this.inputsWithFileKeyword.add(inputWithFileKeyword);
		} else {

		}
	}

	public void removeInputWithFileKeyword(String inputWithFileKeyword) {
		if (this.inputsWithFileKeyword != null) {
			this.inputsWithFileKeyword.remove(inputWithFileKeyword);
		}
	}

}
