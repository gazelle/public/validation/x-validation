package net.ihe.gazelle.xvalidation.dao;

import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInputQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class ValidatorInputDAO extends DAO<ValidatorInput> {

	private static Logger log = LoggerFactory.getLogger(ValidatorInputDAO.class);

	public ValidatorInputDAO() {
		super();
	}

	public ValidatorInputDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static ValidatorInputDAO instanceWithDefaultEntityManager() {
		return new ValidatorInputDAO();
	}

	public List<ValidatorInput> getInputsForGazelleCrossValidator(GazelleCrossValidatorType validator) {
		ValidatorInputQuery query = new ValidatorInputQuery(entityManager);
		query.gazelleCrossValidator().eq(validator);
		query.referencedObject().keyword().order(true);
		return query.getList();
	}

	@Override
	public Class<ValidatorInput> getEntityClass() {
		return ValidatorInput.class;
	}

	public void remove(ValidatorInput input) {
		try {
			ValidatorInput toRemove = find(input.getId());
			entityManager.remove(toRemove);
			entityManager.flush();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}

	public List<String> getInputKeywordsForValidator(GazelleCrossValidatorType gazelleCrossValidator) {
		ValidatorInputQuery query = new ValidatorInputQuery(entityManager);
		query.gazelleCrossValidator().id().eq(gazelleCrossValidator.getId());
		return query.referencedObject().keyword().getListDistinct();
	}

	public List<ValidatorInput> getInputsFromParents(GazelleCrossValidatorType validator) {
		if (validator.getValidatorParents().isEmpty()) {
			return null;
		} else {
			ValidatorInputQuery query = new ValidatorInputQuery(entityManager);
			List<HQLRestriction> restrictions = buildRestrictionListOnParent(query, validator);
			query.addRestriction(HQLRestrictions.or(restrictions.toArray(new HQLRestriction[restrictions.size()])));
			return query.getList();
		}
	}

	private List<HQLRestriction> buildRestrictionListOnParent(ValidatorInputQuery query, GazelleCrossValidatorType validator) {
		List<HQLRestriction> restrictions = new ArrayList<HQLRestriction>();
		if (!validator.getValidatorParents().isEmpty()) {
			for (CrossValidatorReference reference : validator.getValidatorParents()) {
				restrictions.add(HQLRestrictions.and(
						query.gazelleCrossValidator().affinityDomain().eqRestriction(reference.getAffinityDomain()),
						query.gazelleCrossValidator().name().eqRestriction(reference.getName())));
			}
		}
		return restrictions;
	}

	public ValidatorInput getInputByKeyword(String keyword, Rule rule) {
		ValidatorInputQuery query = new ValidatorInputQuery();
		query.gazelleCrossValidator().eq(rule.getGazelleCrossValidator());
		query.referencedObject().keyword().eq(keyword);
		return query.getUniqueResult();
	}

	public List<ValidatorInput> convertInInputList(List<String> keywordList, Rule rule) {
		List<ValidatorInput> validatorInputList = new ArrayList();
		for (String keyword : keywordList) {
			validatorInputList.add(getInputByKeyword(keyword,rule));
		}
		return validatorInputList;
	}

}
