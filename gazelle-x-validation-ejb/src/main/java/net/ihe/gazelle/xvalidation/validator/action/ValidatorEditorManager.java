package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.action.RemoteXValidator;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.DICOMLibrary;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.model.MessageType;
import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.core.model.ValidatorConfiguration;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInputQuery;
import net.ihe.gazelle.xvalidation.dao.CrossValidatorReferenceDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.NamespaceDAO;
import net.ihe.gazelle.xvalidation.dao.ReferencedObjectDAO;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.component.UITree;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Name("xvalidatorEditorManager")
@Scope(ScopeType.PAGE)
public class ValidatorEditorManager implements Serializable, QueryModifier {

    /**
     *
     */

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RemoteXValidator.class);

    private static final long serialVersionUID = -280923369916967093L;
    private GazelleCrossValidatorType validator;
    private GazelleCrossValidatorType parentToAdd;
    private static final List<SelectItem> DICOM_LIBRARIES;
    private static final List<SelectItem> OBJECT_TYPES;
    private static final List<SelectItem> MSG_TYPES;
    private Namespace namespaceToAdd = new Namespace();
    private ValidatorInput selectedValidatorInput;
    private boolean createANewInput;
    private String newVersion;

    private final Boolean adviseNodeOpened = Boolean.TRUE;
    private List<GazelleTreeNodeImpl<CrossValidatorReference>> rootNodes = new ArrayList<>();


    private FilterDataModel<ValidatorInput> validatorInputs;
    private Filter<ValidatorInput> inputFilter;

    private FilterDataModel<Rule> rules;
    private Filter<Rule> ruleFilter;

    private List<Rule> inheritedRules;

    private List<GazelleCrossValidatorType> alreadyParentValidators;

    private List<String> deprecatedValidatorParentsName;


    static {
        DICOM_LIBRARIES = new ArrayList<SelectItem>();
        OBJECT_TYPES = new ArrayList<SelectItem>();
        MSG_TYPES = new ArrayList<SelectItem>();
        for (DICOMLibrary lib : DICOMLibrary.values()) {
            DICOM_LIBRARIES.add(new SelectItem(lib, lib.getValue()));
        }
        for (Kind kind : Kind.values()) {
            OBJECT_TYPES.add(new SelectItem(kind, kind.getValue()));
        }
        for (MessageType msgtype : MessageType.values()) {
            MSG_TYPES.add(new SelectItem(msgtype, msgtype.getValue()));
        }
    }

    @Create
    public void initEditor() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            Integer id = Integer.decode(params.get("id"));
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().find(id);
            if (alreadyParentValidators == null) {
                alreadyParentValidators = new ArrayList<>();
                computeAlreadyParentValidatorsList();
            } else {
                computeAlreadyParentValidatorsList();
            }
//            if (validator.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX)) {
//                newVersion = String.valueOf(Integer.decode(validator.getVersion().replace(GazelleCrossValidatorType.DEV_SUFFIX, "")) + 1);
//            } else {
//                newVersion = String.valueOf(Integer.decode(validator.getVersion()) + 1);
//            }
            if (validator == null) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, id + " is not a valid identifier");
            } else if (validator.isAvailable()) {
                GuiMessage.logMessage(StatusMessage.Severity.WARN,
                        "You are not allowed to edit an available validator, use the update button");
            } else if (validator.isDeprecated()) {
                GuiMessage.logMessage(StatusMessage.Severity.WARN, "This validator is deprecated, you might not want to update it");
            }
            if (validator.getOid() == null) {
                validator.setOid(id);
                validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
            }
        } else {
            // we are creating a new validator
            validator = new GazelleCrossValidatorType();
            alreadyParentValidators = new ArrayList<>();
            validator.setStatus(Status.UNDERDEV);
            validator.getConfiguration().setAssertionManagerUrl(PreferenceService.getString("assertion_manager_url"));
            validator.getConfiguration().setValueSetProviderUrl(PreferenceService.getString("svs_repository_url"));
        }
    }

    public void save() {
        validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
        if (validator.getOid().equals("Not available")) {
//            newVersion = String.valueOf(Integer.decode(validator.getVersion()) + 1);
            validator.setOid(validator.getId());
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
        }
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Changes have been correctly saved");
    }

    public String cancel() {
        validator = null;
        return XValidationPages.ADMIN_VALIDATORS.getLink();
    }


    public String enableValidator() {
        if (validator.isUnderDevelopment() && validator.getVersion().isEmpty()) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "A version is mandatory");
            return null;
        } else if (validator.isUnderMaintenance()) {
            String oldVersion = validator.getVersion().replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
            if (newVersion == null || newVersion.isEmpty() || newVersion.equals(oldVersion)) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "You are requested to provide a new version for this validator");
                return null;
            } else {
                validator.setVersion(newVersion);
            }
        }
        changeStatus(Status.AVAILABLE);
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "This validator is now ready to be used");
        return XValidationPages.DOC_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
    }

    public String deprecateValidator() {
        changeStatus(Status.DEPRECATED);
        GuiMessage.logMessage(StatusMessage.Severity.WARN, "Users will not be able to select this validator anymore");
        return XValidationPages.DOC_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
    }

    private void changeStatus(Status newStatus) {
        validator.setStatus(newStatus);
        GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
    }

    public List<GazelleCrossValidatorType> availableValidators() {
        List<GazelleCrossValidatorType> availableValidatorsList = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
                .getValidatorsToInheritFrom(validator);
        availableValidatorsList.removeAll(alreadyParentValidators);
        return availableValidatorsList;
    }

    private void computeAlreadyParentValidatorsList() {
        for (CrossValidatorReference reference : validator.getValidatorParents()) {
            GazelleCrossValidatorType validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorToReference(reference);
            if (validator.getOid() != null) {
                if (validator.getStatus() != Status.DEPRECATED) {
                    alreadyParentValidators.add(GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorToReference(reference));
                }
            }
        }
    }

    public List<ReferencedObject> getAvailableReferencedObjects() {
        return ReferencedObjectDAO.instanceWithDefaultEntityManager().getAllEntities();
    }

    public void onSelectParentListener(ValueChangeEvent changeEvent) {
        if (changeEvent.getNewValue() instanceof GazelleCrossValidatorType) {
            GazelleCrossValidatorType parent = (GazelleCrossValidatorType) changeEvent.getNewValue();
            CrossValidatorReference reference = CrossValidatorReferenceDAO.instanceWithDefaultEntityManager().getReferenceToValidator(parent);
            alreadyParentValidators.add(parent);
            validator.addValidatorParent(reference);
            List<ValidatorInput> inputsForParent = ValidatorInputDAO.instanceWithDefaultEntityManager()
                    .getInputsForGazelleCrossValidator(parent);
            for (ValidatorInput parentInput : inputsForParent) {
                boolean addInput = true;
                for (ValidatorInput input : validator.getValidatedObjects()) {
                    if (input.getKeyword().equals(parentInput.getKeyword())) {
                        addInput = false;
                        if (parentInput.getMinQuantity() > input.getMinQuantity()) {
                            input.setMinQuantity(parentInput.getMinQuantity());
                            GuiMessage.logMessage(StatusMessage.Severity.INFO,
                                    "min cardinality for " + input.getKeyword() + " has been increased to "
                                            + parentInput.getMinQuantity());
                        }
                    }
                }
                GuiMessage.logMessage(StatusMessage.Severity.INFO, parentInput.getKeyword() + " has been added to the list of inherited " +
                        "inputs");
                if (addInput) {
                    ValidatorInput toAdd = new ValidatorInput(parentInput);
                    toAdd.setGazelleCrossValidator(validator);
                    validator.addValidatedObjects(toAdd);
                    GuiMessage.logMessage(StatusMessage.Severity.INFO, toAdd.getKeyword() + " has been added to the list of inherited inputs");
                }
            }
            save();
        }
    }

    public ValidatorConfiguration getValidatorConfiguration() {
        if (validator != null) {
            return validator.getConfiguration();
        } else {
            return null;
        }
    }

    public void setValidatorConfiguration(ValidatorConfiguration validatorConfiguration) {
        if (validator != null) {
            validator.setConfiguration(validatorConfiguration);
        }
    }

    public List<Namespace> getValidatorNamespaces() {
        if (validator != null) {
            return getValidatorConfiguration().getNamespaces();
        } else {
            return null;
        }
    }

    public void addNamespaceToList() {
        if (namespaceToAdd != null && namespaceToAdd.isValid()) {
            getValidatorConfiguration().addNamespace(namespaceToAdd);
            save();
            namespaceToAdd = new Namespace();
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Both the prefix and the uri are mandatory");
        }
    }

    public List<String> getAlreadyUsedNamespaceUri() {
        return NamespaceDAO.instanceWithDefaultEntityManager().getUsedNamespaceUri();
    }

    public void removeInput() {
        if (selectedValidatorInput != null) {
            selectedValidatorInput = ValidatorInputDAO.instanceWithDefaultEntityManager().saveEntity(
                    selectedValidatorInput);
            validator.removeValidatedObjects(selectedValidatorInput);
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
            //ValidatorInputDAO.instanceWithDefaultEntityManager().saveEntity(selectedValidatorInput);
            ValidatorInputDAO.instanceWithDefaultEntityManager().remove(selectedValidatorInput);
            selectedValidatorInput = null;
        }
    }

    public void addValidatorInput() {
        if (selectedValidatorInput != null) {
            selectedValidatorInput = ValidatorInputDAO.instanceWithDefaultEntityManager().saveEntity(
                    selectedValidatorInput);
            validator.addValidatedObjects(selectedValidatorInput);
            selectedValidatorInput = null;
        }
    }

    public String redirectToReferencedObjectDoc(ValidatorInput input) {
        return XValidationPages.DOC_REFERENCE.getSeamLink() + "?keyword=" + input.getReferencedObject().getKeyword();
    }

    public String editValidatorInput(ValidatorInput input) {
        return XValidationPages.ADMIN_EDIT_INPUT.getSeamLink() + "?validatorId=" + validator.getId() + "&id=" + input.getId();
    }

    public FilterDataModel<ValidatorInput> getValidatorInputs() {
        return new FilterDataModel<ValidatorInput>(getInputFilter()) {
            @Override
            protected Object getId(ValidatorInput validatorInput) {
                return validatorInput.getId();
            }
        };
    }

    public Filter<ValidatorInput> getInputFilter() {
        if (inputFilter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.inputFilter = new Filter(this.getHqlCriterionsForInput(), params);
        }
        return this.inputFilter;
    }

    private HQLCriterionsForFilter getHqlCriterionsForInput() {
        ValidatorInputQuery query = new ValidatorInputQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("objectType", query.referencedObject().objectType());
        criterion.addPath("keyword", query.referencedObject().keyword());
        criterion.addPath("description", query.referencedObject().description());
        criterion.addQueryModifier(this);
        return criterion;
    }

    public void resetInputFilter() {
        if (this.inputFilter != null) {
            this.inputFilter.clear();
        }
    }

    @Override
    public void modifyQuery(HQLQueryBuilder hqlQueryBuilder, Map map) {
        ValidatorInputQuery inputQuery = new ValidatorInputQuery();
        RuleQuery ruleQuery = new RuleQuery();
        if (validator != null) {
            hqlQueryBuilder.addRestriction(inputQuery.gazelleCrossValidator().eqRestriction(validator));
            hqlQueryBuilder.addRestriction(ruleQuery.gazelleCrossValidator().eqRestriction(validator));
        }
    }

    public FilterDataModel<Rule> getRules() {
        return new FilterDataModel<Rule>(getRuleFilter()) {
            @Override
            protected Object getId(Rule rule) {
                return rule.getId();
            }
        };
    }

    public Filter<Rule> getRuleFilter() {
        if (ruleFilter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.ruleFilter = new Filter(this.getHqlCriterionsForRule(), params);
        }
        return this.ruleFilter;
    }

    private HQLCriterionsForFilter getHqlCriterionsForRule() {
        RuleQuery query = new RuleQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("keyword", query.keyword());
        criterion.addPath("version", query.version());
        criterion.addQueryModifier(this);
        return criterion;
    }

    public void resetRuleFilter() {
        if (this.ruleFilter != null) {
            this.ruleFilter.clear();
        }
    }


    public String addRule() {
        return XValidationPages.ADMIN_EDIT_RULE.getSeamLink() + "?validatorId=" + validator.getId();
    }

    public String editRule(Rule rule) {
        return addRule() + "&id=" + rule.getId();
    }

    public void removeRule(Rule rule) {
        rule.setStatus(RuleStatus.DEPRECATED);
        RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
    }

    public void initializeValidatorInput() {
        selectedValidatorInput = new ValidatorInput();
        selectedValidatorInput.setGazelleCrossValidator(validator);
        createANewInput = false;
    }

    public void removeNamespace(Namespace toRemove) {
        getValidatorConfiguration().removeNamespace(toRemove);
    }

    public List<String> getAvailableAffinityDomains() {
        return GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getUsedAffinityDomains();
    }

    public void validateValidatorName(FacesContext context, UIComponent component, Object value)
            throws ValidatorException {
        if (value instanceof String) {
            String name = (String) value;
            Integer validatorId = validator.getId();
            String affinityDomain = validator.getAffinityDomain();
            if (!GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().isNameAvailable(name, affinityDomain,
                    validatorId)) {
                throw new ValidatorException(
                        new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Gazelle X Validator name shall be unique accross the application for an affinity domain and this one is already " +
                                        "used",
                                null));
            }
        }
    }

    public void validateReferencedObjectKeyword(FacesContext context, UIComponent component, Object value)
            throws ValidatorException {
        if (value instanceof String) {
            String keyword = (String) value;
            if (!ReferencedObjectDAO.instanceWithDefaultEntityManager().isKeywordAvailable(keyword, null)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Keyword of the input shall be unique accross the application and this one is already used",
                        null));
            }
        }
    }

    public String getSvsRepositoryDefaultUrl() {
        return PreferenceService.getString("svs_repository_url");
    }

    public void removeParent(CrossValidatorReference parent) {
        validator.removeValidatorParent(parent);
        GuiMessage.logMessage(StatusMessage.Severity.WARN, parent.getName() + " removed from Inherited validators.");
        GuiMessage.logMessage(StatusMessage.Severity.WARN, "Inherited inputs and rules have been unlinked from this validator.");
        if (!alreadyParentValidators.isEmpty()) {
            alreadyParentValidators.remove(GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorFromName(parent.getName()));
        }
        parentToAdd = null;
        save();
    }

    public GazelleCrossValidatorType getValidator() {
        return validator;
    }

    public void setValidator(GazelleCrossValidatorType validator) {
        this.validator = validator;
    }

    public GazelleCrossValidatorType getParentToAdd() {
        return parentToAdd;
    }

    public void setParentToAdd(GazelleCrossValidatorType parentToAdd) {
        this.parentToAdd = parentToAdd;
    }

    public List<SelectItem> getDicomLibraries() {
        return DICOM_LIBRARIES;
    }

    public List<SelectItem> getMsgTypes() {
        return MSG_TYPES;
    }

    public Namespace getNamespaceToAdd() {
        return namespaceToAdd;
    }

    public void setNamespaceToAdd(Namespace namespaceToAdd) {
        this.namespaceToAdd = namespaceToAdd;
    }

    public ValidatorInput getSelectedValidatorInput() {
        return selectedValidatorInput;
    }

    public void setSelectedValidatorInput(ValidatorInput selectedValidatorInput) {
        this.selectedValidatorInput = selectedValidatorInput;
    }

    public boolean isCreateANewInput() {
        return createANewInput;
    }

    public void setCreateANewInput(boolean createANewInput) {
        this.createANewInput = createANewInput;
        if (createANewInput) {
            selectedValidatorInput.setReferencedObject(new ReferencedObject());
            selectedValidatorInput.getReferencedObject().setObjectType(Kind.CDA);
        } else {
            selectedValidatorInput.setReferencedObject(null);
        }
    }

    public List<SelectItem> getObjectTypes() {
        return OBJECT_TYPES;
    }

    public boolean isValidatorReady() {
        if (validator.getId() == null) {
            return false;
        } else if (validator.getRules().isEmpty()) {
            return false;
        } else {
            List<Rule> availableRules = RuleDAO.instanceWithDefaultEntityManager()
                    .getActiveRulesForValidator(validator);
            return (!availableRules.isEmpty());
        }
    }

    public String getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(String newVersion) {
        this.newVersion = newVersion;
    }

    public Boolean adviseNodeOpened(final UITree tree) {
        return this.adviseNodeOpened;
    }

//    public GazelleListDataModel<ValidatorInput> getInheritedInputs() {
//        this.inheritedInputs = ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsFromParents(validator);
//        if (this.inheritedInputs != null) {
//            return new GazelleListDataModel<>(this.inheritedInputs);
//        } else {
//            return null;
//        }
//    }

    public GazelleListDataModel<Rule> getInheritedRules() {
        this.inheritedRules = RuleDAO.instanceWithDefaultEntityManager().getRulesFromParents(validator);
        if (this.inheritedRules != null) {
            return new GazelleListDataModel<>(this.inheritedRules);
        } else {
            return null;
        }
    }

    public List<String> getDeprecatedValidatorParentsName() {
        deprecatedValidatorParentsName = new ArrayList<>();
        List<CrossValidatorReference> validatorParentReferences = validator.getValidatorParents();
        return GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().
                listDeprecatedParentValidatorsName(deprecatedValidatorParentsName, validatorParentReferences);
    }

    public void setDeprecatedValidatorParentsName(List<String> deprecatedValidatorParentsName) {
        this.deprecatedValidatorParentsName = deprecatedValidatorParentsName;
    }


    public List<String> checkInputUsage() {
        List<String> rules = new ArrayList<String>();
        if (selectedValidatorInput != null) {
            for (Rule currentRule : getInheritedRules()) {
                String keyword = doesRuleUseInput(currentRule, selectedValidatorInput.getKeyword());
                if (keyword != null) {
                    rules.add(keyword);
                }
            }
            for (Rule currentRule : validator.getRules()) {
                String keyword = doesRuleUseInput(currentRule, selectedValidatorInput.getKeyword());
                if (keyword != null) {
                    rules.add(keyword);
                }
            }
        }
        Collections.sort(rules);
        return rules;
    }

    private static String doesRuleUseInput(Rule currentRule, String testedKeyword) {
        if (currentRule.getAppliesTo() != null && currentRule.getAppliesTo().contains(testedKeyword)) {
            return currentRule.getKeyword() + "(" + currentRule.getStatus().value() + ")";
        } else {
            return null;
        }
    }

    public void markInputOptional() {
        if (selectedValidatorInput != null) {
            selectedValidatorInput.setMinQuantity(0);
            ValidatorInputDAO.instanceWithDefaultEntityManager().saveEntity(selectedValidatorInput);
            selectedValidatorInput = null;
        }
    }

    public GazelleTreeNodeImpl<CrossValidatorReference> buildTree() {
        final GazelleTreeNodeImpl<CrossValidatorReference> tree = new GazelleTreeNodeImpl<>();
        tree.setData(CrossValidatorReferenceDAO.instanceWithDefaultEntityManager().getReferenceToValidator(validator));
        int index = 0;
        List<CrossValidatorReference> analyzedReferences = new ArrayList<>();
        for (final CrossValidatorReference childReference : validator.getValidatorParents()) {
            GazelleTreeNodeImpl<CrossValidatorReference> node = new GazelleTreeNodeImpl<>();
            final List tmpList = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorToReference(childReference).getValidatorParents();
            if (tmpList != null && !tmpList.isEmpty() && !analyzedReferences.contains(childReference)) {
                analyzedReferences.add(childReference);
                node = this.processNodeList(childReference, analyzedReferences);
                tree.addChild(index, node);
            }
            else {
                node.setData(childReference);
                tree.addChild(index, node);
            }
            index++;
            rootNodes.add(node);
        }

        return tree;
    }

    private GazelleTreeNodeImpl<CrossValidatorReference> processNodeList(final CrossValidatorReference reference, List<CrossValidatorReference> analyzedReferences) {
        final GazelleTreeNodeImpl<CrossValidatorReference> tree = new GazelleTreeNodeImpl<>();
        tree.setData(reference);
        GazelleCrossValidatorType validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorToReference(reference);
        int index = 0;
        for (final CrossValidatorReference childReference : validator.getValidatorParents()) {
            final List tmpList = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorToReference(childReference).getValidatorParents();
            if (tmpList != null && !tmpList.isEmpty() && !analyzedReferences.contains(childReference)) {
                analyzedReferences.add(childReference);
                tree.addChild(index, this.processNodeList(childReference, analyzedReferences));
            } else {
                final GazelleTreeNodeImpl<CrossValidatorReference> node = new GazelleTreeNodeImpl<>();
                node.setData(childReference);
                tree.addChild(index, node);
            }
            index++;
        }
        return tree;
    }

    public boolean isDirectValidatorParent(GazelleTreeNodeImpl<CrossValidatorReference> node, CrossValidatorReference reference) {
        return validator.getValidatorParents().contains(reference) && rootNodes.contains(node);
    }
}
