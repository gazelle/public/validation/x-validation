package net.ihe.gazelle.xvalidation.rule.action;

import java.util.List;
import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("ruleAdministrationManager")
@Scope(ScopeType.PAGE)
public class RuleAdministrationManager extends AbstractRuleBrowser {

    /**
     *
     */
    private static final long serialVersionUID = 7166081396049163857L;

    private boolean withFailedUT = false;
    private boolean withNoUT = false;
    private boolean withNoAssertion = false;

    public boolean isWithFailedUT() {
        return withFailedUT;
    }

    public void setWithFailedUT(boolean withFailedUT) {
        this.withFailedUT = withFailedUT;
    }

    public boolean isWithNoUT() {
        return withNoUT;
    }

    public void setWithNoUT(boolean withNoUT) {
        this.withNoUT = withNoUT;
    }

    public boolean isWithNoAssertion() {
        return withNoAssertion;
    }

    public void setWithNoAssertion(boolean withNoAssertion) {
        this.withNoAssertion = withNoAssertion;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    protected void addSpecificHQLCriterions(HQLCriterionsForFilter<Rule> criterions, RuleQuery query) {
        criterions.addPath("modifier", query.lastModifier());
        criterions.addPath("timestamp", query.lastModifiedDate());
        criterions.addPath("status", query.status());
    }


    @Override
    public String listValidators() {
        return XValidationPages.ADMIN_VALIDATORS.getLink();
    }

    @Override
    public String listReferencedObjects() {
        return XValidationPages.ADMIN_REFERENCES.getLink();
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Rule> queryBuilder, Map<String, Object> filterValuesApplied) {
        if (withFailedUT) {
            RuleUnitTestQuery utQuery = new RuleUnitTestQuery();
            utQuery.lastResult().eq(false);
            List<Integer> failed = utQuery.testedRule().id().getListDistinct();
            if (failed != null && !failed.isEmpty()) {
                RuleQuery query = new RuleQuery();
                queryBuilder.addRestriction(query.id().inRestriction(failed));
            }
        }
        if (withNoUT) {
            RuleUnitTestQuery utQuery = new RuleUnitTestQuery();
            List<Integer> covered = utQuery.testedRule().id().getListDistinct();
            RuleQuery query = new RuleQuery();
            queryBuilder.addRestriction(HQLRestrictions.not(query.id().inRestriction(covered)));
        }
        if (withNoAssertion) {
            RuleQuery query = new RuleQuery();
            queryBuilder.addRestriction(query.assertions().isEmptyRestriction());
        }
    }

}
