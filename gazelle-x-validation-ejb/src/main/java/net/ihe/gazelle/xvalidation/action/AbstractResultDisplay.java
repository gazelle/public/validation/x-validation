package net.ihe.gazelle.xvalidation.action;

import net.ihe.gazelle.callingtools.common.action.CallingToolInterface;
import net.ihe.gazelle.callingtools.common.model.CallingTool;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.report.ValidationResultType;
import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import org.apache.james.mime4j.util.CharsetUtil;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractResultDisplay implements CallingToolInterface, Serializable {

	private CrossValidationLog validationLog;
	private boolean userAllowed = true;
	private static Logger log = LoggerFactory.getLogger(AbstractResultDisplay.class);

	private List<File> filesList = new ArrayList<>();

	public List<File> getFilesList() {
		return filesList;
	}


	protected boolean currentUserIsAllowedToAccessLog(String privacyKey) {
		return validationLog.currentUserIsAuthorized(privacyKey);
	}

	public void setResultPublic() {
		validationLog.markPublic();
		CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(validationLog);
	}

	public void setResultPrivate() {
		validationLog.markPrivate();
		CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(validationLog);
	}

	public void setResultProtected() {
		validationLog.protectByKey();
		CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(validationLog);
	}

	public boolean currentUserCanChangeVisibility() {
		return (Identity.instance().isLoggedIn() && (Identity.instance().getCredentials().getUsername()
				.equals(validationLog.getUsername()) || Identity.instance().hasRole("admin_role")));
	}

	protected void setDetailedResult(String result) {
		validationLog.setDetailedResult(result);
	}

	protected void setValidationResult(ValidationResultType status) {
		validationLog.setResult(status);
	}

	public CrossValidationLog getValidationLog() {
		return validationLog;
	}

	public void setValidationLog(CrossValidationLog validationLog) {
		this.validationLog = validationLog;
	}

	public boolean isUserAllowed() {
		return userAllowed;
	}

	public void setUserAllowed(boolean userAllowed) {
		this.userAllowed = userAllowed;
	}

	private boolean deleteFilesInList() {
		List<File> fileList = getFilesList();
		for (File file : fileList) {
			if (!file.getName().contains("extracted_attachment")) {
				boolean isDeleted = file.delete();
				if (!isDeleted) {
					log.error("Unable to delete " + file.getParentFile().getAbsolutePath());
					return false;
				}
			}
		}
		fileList.removeAll(fileList);
		return fileList.isEmpty();
	}

	@Override
	public void sendPermaLink() {

		boolean isFileListCleared = deleteFilesInList();
		if (isFileListCleared) {
			log.info("All files in map deleted during x validation");
		}

		if (getValidationLog().getCallingToolOid() != null && getValidationLog().getExternalId() != null) {

			final CallingTool tool = CallingTool.getToolByOid(getValidationLog().getCallingToolOid());
			if (tool != null && tool.getUrl() != null && !tool.getUrl().isEmpty()) {
				final StringBuilder builder = new StringBuilder(tool.getUrl());
				final String obj = DatatypeConverter.printBase64Binary(getValidationLog().getOid().getBytes(CharsetUtil.UTF_8));
				if (getValidationLog().getMessageTypeFromProxy() != null) {
                    if (!tool.getUrl().endsWith("/")) {
                        builder.append("/");
                    }
					builder.append("messages/");
					computeMessageProxyType();
					builder.append(getValidationLog().getMessageTypeFromProxy()).append(".seam");
					builder.append("?id=");
				}
				builder.append(getValidationLog().getExternalId());
				builder.append("&logoid=");
				builder.append(obj);
				FacesManager.instance().redirectToExternalURL(builder.toString());
			} else {
				FacesMessages.instance()
						.add("OID " + getValidationLog().getCallingToolOid() + " does not match any known tool");
			}
		}
	}

	private void computeMessageProxyType() {
		if (getValidationLog().getMessageTypeFromProxy().equals("HTTP")) {
			getValidationLog().setMessageTypeFromProxy("http");
		} else if (getValidationLog().getMessageTypeFromProxy().equals("RAW")) {
			getValidationLog().setMessageTypeFromProxy("raw");
		} else if (getValidationLog().getMessageTypeFromProxy().equals("HL7")) {
			getValidationLog().setMessageTypeFromProxy("hl7");
		} else if (getValidationLog().getMessageTypeFromProxy().equals("DICOM")) {
			getValidationLog().setMessageTypeFromProxy("dicom");
		} else if (getValidationLog().getMessageTypeFromProxy().equals("SYSLOG")) {
			getValidationLog().setMessageTypeFromProxy("syslog");
		} else {
			log.error("Message Type from proxy undetected, please add that type in the AbstractResultDisplay class");
			GuiMessage.logMessage(StatusMessage.Severity.ERROR,"Message Type from proxy undetected, please add that type in the AbstractResultDisplay class");
		}
	}

	@Override
	public boolean sendBackResultToTool() {

		if (getValidationLog().getCallingToolOid() ==  null || getValidationLog().getExternalId() == null) {
			return(false);
		} else {
			return(true);
		}
	}

	public void reset() {
		validationLog = null;
		userAllowed = true;
	}

}
