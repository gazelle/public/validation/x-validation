package net.ihe.gazelle.xvalidation.provider;

import net.ihe.gazelle.common.LinkDataProvider;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import net.ihe.gazelle.xvalidation.rule.action.RuleEditorManager;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.List;

@MetaInfServices(LinkDataProvider.class)
    public class CrossValidatorLinkDataProvider implements LinkDataProvider {

	public static final CrossValidatorLinkDataProvider instance() {
		return new CrossValidatorLinkDataProvider();
	}

	private static List<Class<?>> supportedClasses;

	static {
		supportedClasses = new ArrayList<Class<?>>();
		supportedClasses.add(GazelleCrossValidatorType.class);
		supportedClasses.add(Rule.class);
		supportedClasses.add(RuleEditorManager.class);
		supportedClasses.add(ReferencedObject.class);
		supportedClasses.add(CrossValidationLog.class);
		supportedClasses.add(RuleUnitTest.class);
		supportedClasses.add(CrossValidatorReference.class);
	}

	@Override
	public List<Class<?>> getSupportedClasses() {
		return supportedClasses;
	}

	@Override
	public String getLabel(Object o, boolean detailed) {
		StringBuilder label = new StringBuilder();
		if (o instanceof GazelleCrossValidatorType) {
			GazelleCrossValidatorType validator = (GazelleCrossValidatorType) o;
			label.append(validator.getName());
			label.append(" - ");
			label.append(validator.getAffinityDomain());
			if (detailed) {
				label.append(" (");
				label.append(validator.getVersion());
				label.append(')');
			}
		} else if (o instanceof Rule) {
			Rule rule = (Rule) o;
			label.append(rule.getKeyword());
			if (detailed) {
				label.append(" (");
				label.append(rule.getVersion());
				label.append(')');
			}
		} else if (o instanceof ReferencedObject) {
			ReferencedObject ro = (ReferencedObject) o;
			label.append(ro.getKeyword());
			label.append(" (");
			label.append(ro.getObjectType().getValue());
			label.append(')');
		} else if (o instanceof CrossValidationLog) {
			CrossValidationLog validationLog = (CrossValidationLog) o;
			label.append(validationLog.getOid());
		} else if (o instanceof RuleUnitTest){
			RuleUnitTest ut = (RuleUnitTest) o;
			label.append(ut.getKeyword());
		} else if (o instanceof CrossValidatorReference) {
			CrossValidatorReference ref = (CrossValidatorReference) o;
			label.append(ref.getName());
			label.append(" - ");
			label.append(ref.getAffinityDomain());
		} else if (o instanceof RuleEditorManager) {
			GazelleCrossValidatorType validator = ((RuleEditorManager) o).getNewRule().getGazelleCrossValidator();
			label.append(validator.getName());
			label.append(" - ");
			label.append(validator.getAffinityDomain());
			if (detailed) {
				label.append(" (");
				label.append(validator.getVersion());
				label.append(')');
			}
		}
		return label.toString();
	}

	@Override
	public String getLink(Object o) {
		StringBuilder url = new StringBuilder();
		if (o instanceof GazelleCrossValidatorType) {
			GazelleCrossValidatorType validator = (GazelleCrossValidatorType) o;
			url.append(XValidationPages.DOC_VALIDATOR.getSeamLink());
			url.append("?id=");
			url.append(validator.getId());
		} else if (o instanceof Rule) {
			Rule rule = (Rule) o;
			url.append(XValidationPages.DOC_RULE.getSeamLink());
			url.append("?keyword=");
			url.append(rule.getKeyword());
		} else if (o instanceof ReferencedObject) {
			ReferencedObject ro = (ReferencedObject) o;
			url.append(XValidationPages.DOC_REFERENCE.getSeamLink());
			url.append("?keyword=");
			url.append(ro.getKeyword());
		} else if (o instanceof CrossValidationLog) {
			CrossValidationLog validationLog = (CrossValidationLog) o;
			url.append(XValidationPages.VAL_REPORT.getSeamLink());
			url.append("?oid=");
			url.append(validationLog.getOid());
			if (validationLog.getPrivacyKey() != null && !validationLog.getPrivacyKey().isEmpty()){
				url.append("&privacyKey=");
				url.append(validationLog.getPrivacyKey());
			}
		} else if (o instanceof RuleUnitTest){
			RuleUnitTest ut = (RuleUnitTest) o;
			url.append(XValidationPages.ADMIN_RULE_UNIT_TEST.getSeamLink());
			url.append("?unitTest=");
			url.append(ut.getKeyword());
		} else if (o instanceof CrossValidatorReference) {
			CrossValidatorReference ref = (CrossValidatorReference) o;
			url.append(XValidationPages.DOC_VALIDATOR.getSeamLink());
			url.append("?name=");
			url.append(ref.getName());
			url.append("&affinityDomain=");
			url.append(ref.getAffinityDomain());
		} else if (o instanceof RuleEditorManager) {
			GazelleCrossValidatorType validator = ((RuleEditorManager) o).getNewRule().getGazelleCrossValidator();
			url.append(XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink());
			url.append("?id=");
			url.append(validator.getId());
		}
		return url.toString();
	}

	@Override
	public String getTooltip(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
