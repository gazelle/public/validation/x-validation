package net.ihe.gazelle.xvalidation.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;

public class XValInputFile {
    private String role;
    private File file;

    public XValInputFile(String role, File file) {
        this.role = role;
        this.file = file;
    }

    public XValInputFile(FileWrapper fw, URI basedir) throws IOException {
        this(fw.getReference(), new File(basedir.resolve(fw.getReference() + "." + System.currentTimeMillis())));
        file.createNewFile();
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(fw.getDocument());
        }
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
