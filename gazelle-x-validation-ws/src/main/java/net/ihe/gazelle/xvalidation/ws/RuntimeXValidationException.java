package net.ihe.gazelle.xvalidation.ws;

public class RuntimeXValidationException extends RuntimeException {

   private static final long serialVersionUID = -5775738318113117980L;

   public RuntimeXValidationException(Throwable throwable) {
      super(throwable);
   }

}
