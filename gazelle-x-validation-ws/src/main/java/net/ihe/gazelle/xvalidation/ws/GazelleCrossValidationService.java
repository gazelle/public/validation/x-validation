package net.ihe.gazelle.xvalidation.ws;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;
import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.UploadedFileDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.UploadedFile;
import net.ihe.gazelle.xvalidation.utils.ValidationEngineWithDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class GazelleCrossValidationService {

    private static Logger log = LoggerFactory.getLogger(GazelleCrossValidationService.class);
    private String currentuser;

    public GazelleCrossValidationService(String currentuser) {
        this.currentuser = currentuser;
    }

    public GazelleCrossValidatorType getValidatorByName(String validatorName) {
        return GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorFromName(validatorName);
    }

    public String validate(GazelleCrossValidatorType validator, Map<String, List<XValInputFile>> validationInputs) throws GazelleXValidationException {
        if (validationInputs == null || validationInputs.values() == null || validationInputs.values().isEmpty()){
            throw new GazelleXValidationException("You need to provide the files to be validated");
        }

        Map<String,ValidatorInput> validatorInputs = getValidatorInputsForValidator(validator);

        assertInputsCardinalities(validatorInputs,validationInputs);

        CrossValidationLog validation = createValidation(validator, validatorInputs);

        try {
            ValidationEngineWithDB engine = new ValidationEngineWithDB(
                    validator,
                    toValidationInputObjets(validator, validatorInputs, validationInputs));
            engine.validate();
            CrossValidationLogDAO dao = CrossValidationLogDAO.instanceWithDefaultEntityManager();
            validation.setDetailedResult(engine.getReportAsString());
            validation.setResult(engine.getStatus());
            dao.persistEntity(validation);
            CrossValidationLog.setOidFromRootOid(validation);
            dao.saveEntity(validation);
            return validation.getDetailedResult();
        } catch (IOException e) {
            throw new GazelleXValidationException("validation-error",e);
        }
    }


    private Map<String, Object> toValidationInputObjets(GazelleCrossValidatorType validator, Map<String, ValidatorInput> validatorInputs, Map<String, List<XValInputFile>> validationInputs) throws GazelleXValidationException {
        Map<String, Object> filesToValidate = new THashMap<>();
        for (Map.Entry<String, List<XValInputFile>> entry: validationInputs.entrySet()) {
            for (XValInputFile input : entry.getValue()) {
                // TODO handle multiple files by input
                try {
                    filesToValidate.put(input.getRole(),
                                        FileConverter.file2Object(
                                                validatorInputs.get(input.getRole()).getReferencedObject().getObjectType(),
                                                input.getFile().getAbsolutePath(),
                                                validator.getDicomLibrary()));
                } catch (FileNotFoundException e) {
                    throw new GazelleXValidationException("file-conversion-error "+input.getFile().getAbsolutePath(),e);
                }
            }
        }
        return filesToValidate;
    }

    public Map<String, List<XValInputFile>> buildValidationInputs(FileListWrapper fileListWrapper, Map<String,ValidatorInput> validatorInputs) throws GazelleXValidationException {
        Map<String,List<XValInputFile>> validationInputs = new THashMap<>();
        for (FileWrapper fw: fileListWrapper.getFilesToValidate()) {
            ValidatorInput role = validatorInputs.get(fw.getReference());
            if (role==null) {
                throw new GazelleXValidationException("invalid-input-role "+fw.getReference());
            }
            List<XValInputFile> roleInputs = validationInputs.get(fw.getReference());
            if (roleInputs==null) {
                roleInputs = new ArrayList<>();
                validationInputs.put(fw.getReference(),
                                    roleInputs);
            }
            try {
                roleInputs.add(new XValInputFile(fw,baseDir().toURI()));
            } catch (IOException e) {
                throw new GazelleXValidationException("input-file-storage-error",e);
            }
        }
        return validationInputs;
    }

    private void assertInputsCardinalities(Map<String,ValidatorInput> inputs, Map<String, List<XValInputFile>> validationInputs) throws GazelleXValidationException {
        for (ValidatorInput input: inputs.values()) {
            List<XValInputFile> roleInputs = validationInputs.get(input.getKeyword());
            if (roleInputs==null && input.getMinQuantity()>0) {
                throw new GazelleXValidationException("missing-mandatory-input "+input.getKeyword());
            } else if (roleInputs!=null && roleInputs.size()<input.getMinQuantity()) {
                throw new GazelleXValidationException("not-enough-input "+input.getKeyword());
            } else if (roleInputs!=null && roleInputs.size()>input.getMaxQuantity()) {
                throw new GazelleXValidationException("too-many-inputs "+input.getKeyword());
            }
        }
    }

    private File baseDir() {
        String baseDirectory = PreferenceService.getString("xvalidator_directory");
        StringBuilder dir = new StringBuilder(baseDirectory);
        dir.append('/');
        dir.append(UploadedFile.BASE_DIR);
        dir.append('/');
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        dir.append(sdf.format(new Date()));
        File baseDir = new File(dir.toString());
        if (!baseDir.mkdirs()) {
            log.error("Unable to create " + baseDir.getAbsolutePath());
        }
        return baseDir;
    }

    public Map<String, ValidatorInput> getValidatorInputsForValidator(GazelleCrossValidatorType validator) {
        Map<String,ValidatorInput> validatorInputs = new THashMap<>();
        for (ValidatorInput vi:ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsForGazelleCrossValidator(validator)) {
            validatorInputs.put(vi.getKeyword(),vi);
        }
        return validatorInputs;
    }

    private CrossValidationLog createValidation(GazelleCrossValidatorType validator, Map<String, ValidatorInput> inputs) throws GazelleXValidationException {
        CrossValidationLog validation = new CrossValidationLog();
        List<UploadedFile> validatedFiles = new ArrayList<>();
        validation.setAffinityDomain(validator.getAffinityDomain());
        validation.setGazelleCrossValidator(validator.getName());
        validation.setUsername(currentuser);
        validation = CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(validation);
        File baseDir = baseDir();
        if ((inputs != null) && !inputs.isEmpty()) {
            for (ValidatorInput input : inputs.values()) {
                UploadedFile upload = new UploadedFile(input, baseDir.getAbsolutePath());
                upload.setLog(validation);
                validatedFiles.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(upload));
            }
        } else {
            throw new GazelleXValidationException("no-input-defined");
        }
        validation.setValidatedFiles(validatedFiles);
        return validation;
    }
}
