<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">

    <xs:element name="GazelleCrossValidator">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="Description" type="xs:string"/>
                <xs:element maxOccurs="unbounded" name="ValidatedObject" type="ValidatorInput"
                            minOccurs="2"/>
                <xs:element maxOccurs="unbounded" minOccurs="0" name="ValidatorParent"
                            type="CrossValidatorDescription"/>
                <xs:element minOccurs="0" name="Configuration" type="ValidatorConfiguration"/>
                <xs:element maxOccurs="unbounded" name="Rule" type="Rule"/>
            </xs:sequence>
            <xs:attribute name="name" type="xs:string" use="required"/>
            <xs:attribute name="version" type="xs:string" use="required"/>
            <xs:attribute name="status" type="status" use="required"/>
            <xs:attribute name="oid" type="xs:string" use="required"/>
            <xs:attribute name="lastModifier" type="xs:string" use="required"/>
            <xs:attribute name="lastModified" type="xs:dateTime" use="required"/>
            <xs:attribute name="affinityDomain" type="xs:string" use="required"/>
        </xs:complexType>
    </xs:element>
    <xs:complexType name="objectType">
        <xs:attribute name="name" type="xs:string" use="required"/>
        <xs:attribute name="kind" type="kind" use="required"/>
        <xs:attribute name="extension" type="xs:string" use="required"/>
    </xs:complexType>
    <xs:simpleType name="kind">
        <xs:annotation>
            <xs:documentation>Enum: CDA, HL7v2, XML, DICOM, REST
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="CDA"/>
            <xs:enumeration value="HL7v2"/>
            <xs:enumeration value="XML"/>
            <xs:enumeration value="DICOM"/>
            <xs:enumeration value="REST"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="ReferencedObject">
        <xs:annotation>
            <xs:documentation>Describes the file used as reference for the X
                Validation
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element minOccurs="0" name="xsdLocation" type="xs:string"/>
        </xs:sequence>
        <xs:attribute name="keyword" type="xs:string" use="required"/>
        <xs:attribute name="description" type="xs:string" use="required"/>
        <xs:attribute name="objectType" type="kind" use="required"/>
    </xs:complexType>
    <xs:simpleType name="status">
        <xs:annotation>
            <xs:documentation>Status of this validator : UNDER DEVELOPMENT,
                AVAILABLE FOR TESTING,
                DEPRECATED
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="UNDERDEV"/>
            <xs:enumeration value="AVAILABLE"/>
            <xs:enumeration value="DEPRECATED"/>
            <xs:enumeration value="MAINTENANCE"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="Rule">
        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="Assertion" type="assertion">
                <xs:annotation>
                    <xs:documentation>for requirement coverage</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="Expression" type="Expression"/>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="AppliesTo" type="xs:string">
                <xs:annotation>
                    <xs:documentation>If element is not present or empty it implies that all the files declared at
                        validator level are used in this rule; otherwise, lists the keywords of the documents/messages
                        processed with this rule
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="keyword" type="xs:string" use="required"/>
        <xs:attribute name="description" type="xs:string" use="required">
            <xs:annotation>
                <xs:documentation>Displayed to the user in the final report
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="level" type="level" use="required"/>
        <xs:attribute name="version" type="xs:string" use="required"/>
        <xs:attribute name="status" type="ruleStatus" use="required"/>
        <xs:attribute name="lastModifier" type="xs:string" use="required"/>
        <xs:attribute name="lastModified" type="xs:dateTime" use="required"/>
    </xs:complexType>
    <xs:simpleType name="level">
        <xs:annotation>
            <xs:documentation>level of fault : ERROR, WARNING, INFO
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="ERROR"/>
            <xs:enumeration value="WARNING"/>
            <xs:enumeration value="INFO"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="message_type">
        <xs:annotation>
            <xs:documentation>Message type : REQUEST,RESPONSE, ATTACHMENT or MANIFEST
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="REQUEST"/>
            <xs:enumeration value="RESPONSE"/>
            <xs:enumeration value="ATTACHMENT"/>
            <xs:enumeration value="MANIFEST"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="ruleStatus">
        <xs:annotation>
            <xs:documentation>status of the rule : Active (in use), Deactivated
                (not in used),
                Deprecated (no more pertinent)
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="ACTIVE"/>
            <xs:enumeration value="INACTIVE"/>
            <xs:enumeration value="DEPRECATED"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="assertion">
        <xs:attribute name="idScheme" type="xs:string" use="required"/>
        <xs:attribute name="assertionId" type="xs:string" use="required"/>
    </xs:complexType>
    <xs:complexType name="Locator">
        <xs:attribute name="appliesOn" type="xs:string" use="required"/>
        <xs:attribute name="path" type="xs:string" use="required"/>
    </xs:complexType>
    <xs:complexType name="LogicalOperation">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence maxOccurs="unbounded" minOccurs="2">
                    <xs:group ref="ExpressionSet"/>
                </xs:sequence>
                <xs:attribute name="logicalOperator" use="required">
                    <xs:simpleType>
                        <xs:restriction base="xs:string">
                            <xs:enumeration value="AND"/>
                            <xs:enumeration value="OR"/>
                            <xs:enumeration value="XOR"/>
                        </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="Expression">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:group ref="ExpressionSet"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="BasicOperation">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="Locator1" type="Locator"/>
                    <xs:element name="Locator2" type="Locator"/>
                </xs:sequence>
                <xs:attribute name="basicOperator" use="required">
                    <xs:simpleType>
                        <xs:restriction base="xs:string">
                            <xs:enumeration value="equal"/>
                            <xs:enumeration value="sizeGreaterOrEqual"/>
                            <xs:enumeration value="sizeLessOrEqual"/>
                            <xs:enumeration value="sizeEqual"/>
                            <xs:enumeration value="in"/>
                            <xs:enumeration value="contain"/>
                        </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="DateComparison">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="Locator1" type="Locator"/>
                    <xs:element name="DateFormats1">
                        <xs:complexType>
                            <xs:choice maxOccurs="unbounded">
                                <xs:element name="DateFormatType">
                                    <xs:complexType>
                                        <xs:attribute name="label" use="required">
                                            <xs:simpleType>
                                                <xs:restriction base="xs:string"/>
                                            </xs:simpleType>
                                        </xs:attribute>
                                        <xs:attribute name="value" use="required">
                                            <xs:simpleType>
                                                <xs:restriction base="xs:string"/>
                                            </xs:simpleType>
                                        </xs:attribute>
                                    </xs:complexType>
                                </xs:element>
                            </xs:choice>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="Locator2" type="Locator"/>
                    <xs:element name="DateFormats2">
                        <xs:complexType>
                            <xs:choice maxOccurs="unbounded">
                                <xs:element name="DateFormatType">
                                    <xs:complexType>
                                        <xs:attribute name="label" use="required">
                                            <xs:simpleType>
                                                <xs:restriction base="xs:string"/>
                                            </xs:simpleType>
                                        </xs:attribute>
                                        <xs:attribute name="value" use="required">
                                            <xs:simpleType>
                                                <xs:restriction base="xs:string"/>
                                            </xs:simpleType>
                                        </xs:attribute>
                                    </xs:complexType>
                                </xs:element>
                            </xs:choice>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
                <xs:attribute name="dateOperator" use="required">
                    <xs:simpleType>
                        <xs:restriction base="xs:string">
                            <xs:enumeration value="equal"/>
                            <xs:enumeration value="before"/>
                            <xs:enumeration value="after"/>
                        </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="InValueSet">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="Locator" type="Locator"/>
                </xs:sequence>
                <xs:attribute name="valueSetId" type="xs:string" use="required"/>
                <xs:attribute name="lang" type="xs:string"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="AbstractExpression"/>
    <xs:complexType name="Present">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="Locator" type="Locator"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:group name="ExpressionSet">
        <xs:choice>
            <xs:element name="Not" type="Not"/>
            <xs:element name="InValueSet" type="InValueSet"/>
            <xs:element name="Present" type="Present"/>
            <xs:element name="LogicalOperation" type="LogicalOperation"/>
            <xs:element name="BasicOperation" type="BasicOperation"/>
            <xs:element name="ForEach" type="ForEach"/>
            <xs:element name="True" type="True"/>
            <xs:element name="DateComparison" type="DateComparison"/>
            <xs:element name="MathComparison" type="MathComparison"/>
        </xs:choice>
    </xs:group>
    <xs:complexType name="ForEach">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="Locator" type="Locator"/>
                    <xs:group ref="ExpressionSet"/>
                </xs:sequence>
                <xs:attribute name="var" type="xs:string" use="required">
                    <xs:annotation>
                        <xs:documentation>
                            the variable for walking through
                            the returned elements
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="Not">
        <xs:complexContent>
            <xs:extension base="Expression"/>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="ValidatorInput">
        <xs:sequence>
            <xs:element name="ReferencedObject" type="ReferencedObject"/>
        </xs:sequence>
        <xs:attribute default="1" name="minQuantity" type="xs:integer">
            <xs:annotation>
                <xs:documentation>How many of this file are required at least
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute default="1" name="maxQuantity" type="xs:integer">
            <xs:annotation>
                <xs:documentation>1 is the default value, 0 means unlimited
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="message_type" type="message_type" use="required"/>
    </xs:complexType>
    <xs:complexType name="ValidatorConfiguration">
        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="NamespaceDeclaration"
                        type="Namespace"/>
            <xs:element minOccurs="0" name="ValueSetProviderUrl" type="xs:string">
                <xs:annotation>
                    <xs:documentation>if not set default is either http://gazelle.ihe.net either the default value from
                        DB
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element minOccurs="0" name="AssertionManagerUrl" type="xs:string">
                <xs:annotation>
                    <xs:documentation>URL of the instance of Assertion Manager owning the assertions covered by this
                        validator
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element minOccurs="0" name="DICOMLibrary">
                <xs:annotation>
                    <xs:documentation>The DICOM library used to dump the DICOM object into XML. This also implies that
                        the XPath in rules are based on the XML schema defined by this library
                    </xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="dcm4che2"/>
                        <xs:enumeration value="pixelmed"/>
                        <xs:enumeration value="NA"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="Namespace">
        <xs:annotation>
            <xs:documentation>declares the namespaces which are used in the rules
                (XPath)
            </xs:documentation>
        </xs:annotation>
        <xs:attribute name="prefix" type="xs:string" use="required"/>
        <xs:attribute name="uri" type="xs:string" use="required"/>
    </xs:complexType>

    <xs:complexType name="True">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:element name="XPath" type="Locator"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="MathOperation">
        <xs:complexContent>
            <xs:extension base="MathMember">
                <xs:sequence>
                    <xs:choice>
                        <xs:element name="SingleMemberLeft" type="SingleMember"/>
                        <xs:element name="ConstantMemberLeft" type="ConstantMember"/>
                        <xs:element name="MathOperationLeft" type="MathOperation"/>
                    </xs:choice>
                    <xs:element name="MathOperator">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="PLUS"/>
                                <xs:enumeration value="MINUS"/>
                                <xs:enumeration value="TIMES"/>
                                <xs:enumeration value="DIVIDE"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:choice>
                        <xs:element name="SingleMemberRight" type="SingleMember"/>
                        <xs:element name="ConstantMemberRight" type="ConstantMember"/>
                        <xs:element name="MathOperationRight" type="MathOperation"/>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="MathComparison">
        <xs:complexContent>
            <xs:extension base="AbstractExpression">
                <xs:sequence>
                    <xs:choice>
                        <xs:element name="SingleMemberLeft" type="SingleMember"/>
                        <xs:element name="ConstantMemberLeft" type="ConstantMember"/>
                        <xs:element name="MathOperationLeft" type="MathOperation"/>
                    </xs:choice>
                    <xs:element name="Comparator">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="equals"/>
                                <xs:enumeration value="lessThan"/>
                                <xs:enumeration value="lessOrEquals"/>
                                <xs:enumeration value="greaterThan"/>
                                <xs:enumeration value="greaterOrEquals"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:choice>
                        <xs:element name="SingleMemberRight" type="SingleMember"/>
                        <xs:element name="ConstantMemberRight" type="ConstantMember"/>
                        <xs:element name="MathOperationRight" type="MathOperation"/>
                    </xs:choice>
                </xs:sequence>
                <xs:attribute name="delta" type="xs:double" use="optional"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="CrossValidatorDescription">
        <xs:attribute name="name" type="xs:string" use="required"/>
        <xs:attribute name="version" type="xs:string" use="optional"/>
        <xs:attribute name="affinityDomain" type="xs:string" use="required"/>
    </xs:complexType>
    <xs:complexType name="SingleMember">
        <xs:complexContent>
            <xs:extension base="MathMember">
                <xs:sequence>
                    <xs:element name="locator" type="Locator"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="MathMember"/>
    <xs:complexType name="ConstantMember">
        <xs:complexContent>
            <xs:extension base="MathMember">
                <xs:attribute name="value" type="xs:double"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
</xs:schema>
