/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import org.w3c.dom.Node;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.Map;

/**
 * <b>Class Description : </b>MathMember<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 25/02/16
 * @class MathMember
 * @package net.ihe.gazelle.xvalidation.core.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MathMember")
@XmlRootElement(name = "MathMember")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "xval_math_member", schema = "public")
@SequenceGenerator(name = "xval_math_member_sequence", sequenceName = "xval_math_member_id_seq", allocationSize = 1)
@DiscriminatorValue("mathMember")
public abstract class MathMember implements CommonOperations {

    private static final long serialVersionUID = 1L;

    private static int generalFakeId = 0;
    private transient int fakeId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "xval_math_member_sequence", strategy = GenerationType.SEQUENCE)
    @XmlTransient
    private Integer id;


    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    @Transient
    private org.w3c.dom.Node xmlNodePresentation;

    public MathMember() {
        super();
        synchronized (MathMember.class) {
            this.fakeId = generalFakeId++;
        }
    }

    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "MathMember", this);
        }
        return xmlNodePresentation;
    }

    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFakeId() {
        return fakeId;
    }

    public abstract MathMember createCopy();
    public abstract Double evaluate(Map<String, Node> files, ValidatorConfiguration configuration, StringBuilder xpathReport)
            throws GazelleXValidationException;

    public abstract String getDisplayName();
}
