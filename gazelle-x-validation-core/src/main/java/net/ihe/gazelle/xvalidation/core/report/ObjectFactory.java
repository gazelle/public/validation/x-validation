package net.ihe.gazelle.xvalidation.core.report;

import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.xvalidation.core.model.Assertion;
import net.ihe.gazelle.xvalidation.core.model.Locator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;

@XmlRegistry
public class ObjectFactory {

	private static final QName NOTIFICATIONSTYPEERROR_QNAME = new QName("", "Error");
	private static final QName NOTIFICATIONSTYPEWARNING_QNAME = new QName("", "Warning");
	private static final QName NOTIFICATIONSTYPEINFO_QNAME = new QName("", "Info");
	private static final QName NOTIFICATIONSTYPEREPORT_QNAME = new QName("", "Report");
	private static final QName NOTIFICATIONSTYPEUNKNOWN_QNAME = new QName("", "Unknown");

	
	private static Logger log = LoggerFactory.getLogger(ObjectFactory.class);
	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link Assertion}
	 * 
	 */
	public Assertion createAssertion() {
		return new Assertion();
	}

	/**
	 * Create an instance of {@link Counters}
	 * 
	 */
	public Counters createCounters() {
		return new Counters();
	}

	/**
	 * Create an instance of {@link GazelleCrossValidationReportType}
	 * 
	 */
	public GazelleCrossValidationReportType createXValidationReportType() {
		return new GazelleCrossValidationReportType();
	}

	/**
	 * Create an instance of {@link Summary}
	 * 
	 */
	public Summary createSummary() {
		return new Summary();
	}

	/**
	 * Create an instance of {@link GazelleCrossValidatorDescription}
	 * 
	 */
	public GazelleCrossValidatorDescription createGazelleXValidator() {
		return new GazelleCrossValidatorDescription();
	}

	/**
	 * Create an instance of {@link CrossValidatorDescription}
	 * 
	 */
	public CrossValidatorDescription createXValidator() {
		return new CrossValidatorDescription();
	}

	/**
	 * Create an instance of {@link NotificationsType}
	 * 
	 */
	public NotificationsType createNotificationsType() {
		return new NotificationsType();
	}

	/**
	 * Create an instance of {@link Notification}
	 * 
	 */
	public Notification createNotification() {
		return new Notification();
	}

	/**
	 * Create an instance of {@link Location}
	 * 
	 */
	public Locator createLocation() {
		return new Locator();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Error", scope = NotificationsType.class)
	public JAXBElement<Error> createNotificationsTypeError(Error value) {
		return new JAXBElement<Error>(NOTIFICATIONSTYPEERROR_QNAME, Error.class, NotificationsType.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Warning", scope = NotificationsType.class)
	public JAXBElement<Warning> createNotificationsTypeWarning(Warning value) {
		return new JAXBElement<Warning>(NOTIFICATIONSTYPEWARNING_QNAME, Warning.class, NotificationsType.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Info", scope = NotificationsType.class)
	public JAXBElement<Info> createNotificationsTypeInfo(Info value) {
		return new JAXBElement<Info>(NOTIFICATIONSTYPEINFO_QNAME, Info.class, NotificationsType.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Report", scope = NotificationsType.class)
	public JAXBElement<Report> createNotificationsTypeReport(Report value) {
		return new JAXBElement<Report>(NOTIFICATIONSTYPEREPORT_QNAME, Report.class, NotificationsType.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Unknown", scope = NotificationsType.class)
	public JAXBElement<Notification> createNotificationsTypeUnknown(Notification value) {
		return new JAXBElement<Notification>(NOTIFICATIONSTYPEUNKNOWN_QNAME, Notification.class,
				NotificationsType.class, value);
	}
	
	public static <T> Node getXmlNodePresentation(String namespace, String localName, T object) {
		Node xmlNodePresentation = null;
		JAXBContext jc;
		Document doc = null;
		try {
			DocumentBuilder db = new XmlDocumentBuilderFactory()
					.setNamespaceAware(true)
					.getBuilder();
			doc = db.newDocument();
		} catch (DocumentBuilderFactoryException e1) {
			log.error(e1.getMessage(), e1);
		}
		try {
			jc = JAXBContext.newInstance("net.ihe.gazelle.xvalidation.core.report");
			Marshaller m = jc.createMarshaller();
			m.marshal(object, doc);
			xmlNodePresentation = doc.getElementsByTagNameNS(namespace, localName).item(0);
		} catch (JAXBException e) {
			try {
				DocumentBuilder db = new XmlDocumentBuilderFactory()
						.setNamespaceAware(true)
						.getBuilder();
				xmlNodePresentation = db.newDocument();
			} catch (Exception ee) {
				log.error(ee.getMessage(), e);
			}
		}
		return xmlNodePresentation;
	}

}