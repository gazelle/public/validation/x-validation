package net.ihe.gazelle.xvalidation.core.exception;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.xvalidation.core.model.Locator;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.report.Aborted;
import net.ihe.gazelle.xvalidation.core.report.Notification;

public class GazelleXValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2972516765555594975L;

	public GazelleXValidationException(String message) {
		super(message);
		this.notifyAdministrator = true;
	}

	public GazelleXValidationException(String message, Exception cause) {
		super(message, cause);
		this.notifyAdministrator = true;
	}

	public GazelleXValidationException(Locator inLocator, String message) {
		super(message);
		this.locator = inLocator;
		this.notifyAdministrator = false;
	}

	public GazelleXValidationException(Locator locator, XPathExpressionException exception) {
		super(exception);
		this.locator = locator;
		this.notifyAdministrator = true;
	}

	private Locator locator;

	private boolean notifyAdministrator;

	public Notification toNotification(Rule rule) {
		Aborted notification = new Aborted();
		notification.setTest(rule.getKeyword());
		notification.setDescription(rule.getDescription());
		notification.setExpression(rule.getTextualExpression());
		notification.setReason(getMessage());
		if (locator != null) {
			notification.addProcessedInput(locator.toString());
		} else {
			notification.setProcessedInputs(rule.getAppliesTo());
		}
		return notification;
	}

	public Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}

	public boolean isNotifyAdministrator() {
		return notifyAdministrator;
	}

	public void setNotifyAdministrator(boolean notifyAdministrator) {
		this.notifyAdministrator = notifyAdministrator;
	}

}
