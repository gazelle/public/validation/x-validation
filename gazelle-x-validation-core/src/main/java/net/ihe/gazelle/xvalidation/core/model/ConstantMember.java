/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import org.w3c.dom.Node;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Map;

/**
 * <b>Class Description : </b>ConstantMember<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 25/02/16
 * @class ConstantMember
 * @package net.ihe.gazelle.xvalidation.core.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"value"})
@XmlRootElement(name = "Constant")
@Entity
@DiscriminatorValue("constantMember")
public class ConstantMember extends MathMember {

    @XmlAttribute(name = "value", required = true)
    @Column(name = "value")
    private Double value;

    @XmlTransient
    @Transient
    private org.w3c.dom.Node xmlNodePresentation;

    public ConstantMember(){
        super();
    }

    public ConstantMember(Double inValue){
        super();
        this.value = inValue;
    }

    public ConstantMember(ConstantMember member){
        super();
        this.value = member.getValue();
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "ConstantMember", this);
        }
        return xmlNodePresentation;
    }

    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    @Override
    public MathMember createCopy() {
        return new ConstantMember(this);
    }

    @Override
    public Double evaluate(Map<String, Node> files, ValidatorConfiguration configuration, StringBuilder xpathReport) {
        xpathReport.append("[" + getDisplayName() + " : " + getValue() +"]");
        return value;
    }

    @Override
    public String getDisplayName() {
        return "Constant";
    }

    @Override
    public String getEditLink() {
        return "/xvalidation/expressions/constant.xhtml";
    }

    @Override
    public void testCorrectness() throws AssertionFailedError {
        Assert.assertFalse("Value shall be a number (Double)", value == null || value.isNaN());
    }

    @Override
    public ArrayList<String> appliesTo() throws AssertionFailedError {
        return new ArrayList<String>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConstantMember)) return false;

        ConstantMember that = (ConstantMember) o;

        return value != null ? value.equals(that.value) : that.value == null;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString(){
        if (value != null){
            return value.toString();
        } else {
            return null;
        }
    }
}
