package net.ihe.gazelle.xvalidation.core.utils;

import java.util.List;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;

public interface ValueSetProvider {

	List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) throws GazelleXValidationException;
}
