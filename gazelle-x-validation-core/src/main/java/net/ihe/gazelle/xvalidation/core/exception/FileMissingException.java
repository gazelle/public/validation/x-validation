package net.ihe.gazelle.xvalidation.core.exception;

import net.ihe.gazelle.xvalidation.core.model.Locator;

public class FileMissingException extends GazelleXValidationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 496549476582334810L;

	public FileMissingException(Locator inLocator) {
		super(inLocator, inLocator.getAppliesOn() + ": this file is missing");
	}

}
