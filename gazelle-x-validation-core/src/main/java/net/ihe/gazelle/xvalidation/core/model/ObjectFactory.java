package net.ihe.gazelle.xvalidation.core.model;

import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;

@XmlRegistry
public class ObjectFactory {

	private static final QName BOOLEANOPERATIONNOT_QNAME = new QName("", "Not");
	private static final QName BOOLEANOPERATIONINVALUESET_QNAME = new QName("", "InValueSet");
	private static final QName BOOLEANOPERATIONPRESENT_QNAME = new QName("", "Present");
	private static final QName BOOLEANOPERATIONBOOLEANOPERATION_QNAME = new QName("", "BooleanOperation");
	private static final QName BOOLEANOPERATIONBASICOPERATION_QNAME = new QName("", "BasicOperation");
	private static final QName BOOLEANOPERATIONFOREACH_QNAME = new QName("", "ForEach");
	
	private static Logger log = LoggerFactory.getLogger(ObjectFactory.class);

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link Assertion}
	 * 
	 */
	public Assertion createAssertion() {
		return new Assertion();
	}

	/**
	 * Create an instance of {@link BasicOperation}
	 * 
	 */
	public BasicOperation createBasicOperation() {
		return new BasicOperation();
	}

	/**
	 * Create an instance of {@link Locator}
	 * 
	 */
	public Locator createLocator() {
		return new Locator();
	}

	/**
	 * Create an instance of {@link LogicalOperation}
	 * 
	 */
	public LogicalOperation createBooleanOperation() {
		return new LogicalOperation();
	}

	/**
	 * Create an instance of {@link Expression}
	 * 
	 */
	public Expression createExpression() {
		return new Expression();
	}

	/**
	 * Create an instance of {@link InValueSet}
	 * 
	 */
	public InValueSet createInValueSet() {
		return new InValueSet();
	}

	/**
	 * Create an instance of {@link Present}
	 * 
	 */
	public Present createPresent() {
		return new Present();
	}

	/**
	 * Create an instance of {@link GazelleCrossValidatorType}
	 * 
	 */
	public GazelleCrossValidatorType createGazelleCrossValidatorType() {
		return new GazelleCrossValidatorType();
	}

	/**
	 * Create an instance of {@link ReferencedObject}
	 * 
	 */
	public ReferencedObject createReferencedObject() {
		return new ReferencedObject();
	}

	/**
	 * Create an instance of {@link Rule}
	 * 
	 */
	public Rule createRule() {
		return new Rule();
	}

	/**
	 * Create an instance of {@link ObjectType}
	 * 
	 */
	public ObjectType createObjectType() {
		return new ObjectType();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Expression }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Not", scope = LogicalOperation.class)
	public JAXBElement<Not> createBooleanOperationNot(Not value) {
		return new JAXBElement<Not>(BOOLEANOPERATIONNOT_QNAME, Not.class, LogicalOperation.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link InValueSet }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "InValueSet", scope = LogicalOperation.class)
	public JAXBElement<InValueSet> createBooleanOperationInValueSet(InValueSet value) {
		return new JAXBElement<InValueSet>(BOOLEANOPERATIONINVALUESET_QNAME, InValueSet.class, LogicalOperation.class,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Present }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Present", scope = LogicalOperation.class)
	public JAXBElement<Present> createBooleanOperationPresent(Present value) {
		return new JAXBElement<Present>(BOOLEANOPERATIONPRESENT_QNAME, Present.class, LogicalOperation.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link LogicalOperation }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "BooleanOperation", scope = LogicalOperation.class)
	public JAXBElement<LogicalOperation> createBooleanOperationBooleanOperation(LogicalOperation value) {
		return new JAXBElement<LogicalOperation>(BOOLEANOPERATIONBOOLEANOPERATION_QNAME, LogicalOperation.class,
				LogicalOperation.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link BasicOperation }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "BasicOperation", scope = LogicalOperation.class)
	public JAXBElement<BasicOperation> createBooleanOperationBasicOperation(BasicOperation value) {
		return new JAXBElement<BasicOperation>(BOOLEANOPERATIONBASICOPERATION_QNAME, BasicOperation.class,
				LogicalOperation.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link BasicOperation }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "ForEach", scope = LogicalOperation.class)
	public JAXBElement<ForEach> createBooleanOperationForEach(ForEach value) {
		return new JAXBElement<ForEach>(BOOLEANOPERATIONFOREACH_QNAME, ForEach.class, LogicalOperation.class, value);
	}

	public static <T> Node getXmlNodePresentation(String namespace, String localName, T object) {
		Node xmlNodePresentation = null;
		JAXBContext jc;

		Document doc = null;
		try {
			DocumentBuilder db = new XmlDocumentBuilderFactory()
					.setNamespaceAware(true)
					.getBuilder();
			doc = db.newDocument();
		} catch (DocumentBuilderFactoryException e1) {
			log.error(e1.getMessage(), e1);
		}
		try {
			jc = JAXBContext.newInstance("net.ihe.gazelle.xvalidation.core.model");
			Marshaller m = jc.createMarshaller();
			m.marshal(object, doc);
			xmlNodePresentation = doc.getElementsByTagNameNS(namespace, localName).item(0);
		} catch (JAXBException e) {
			try {
				DocumentBuilder db = new XmlDocumentBuilderFactory()
						.setNamespaceAware(true)
						.getBuilder();
				xmlNodePresentation = db.newDocument();
			} catch (Exception ee) {
				log.error(ee.getMessage(), e);
			}
		}
		return xmlNodePresentation;
	}

}