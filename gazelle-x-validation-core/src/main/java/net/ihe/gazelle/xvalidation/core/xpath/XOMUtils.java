package net.ihe.gazelle.xvalidation.core.xpath;

import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.utils.IgnoreWhitespaceNodeList;
import net.sf.saxon.dom.DOMNodeList;
import nu.xom.Attribute;
import nu.xom.converters.DOMConverter;
import nu.xom.tests.XOMTestCase;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public final class XOMUtils {

	private XOMUtils() {

	}

	/**
	 * Returns true if each node in list1 is present in list2
	 * 
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static boolean evaluateIn(IgnoreWhitespaceNodeList list1, IgnoreWhitespaceNodeList list2) {
		if ((list1 == null) || (list2 == null)) {
			return false;
		} else {
			for (int index1 = 0; index1 < list1.getLength(); index1++) {
				Node item1 = list1.item(index1);
				boolean matchSingle = false;
				for (int index2 = 0; index2 < list2.getLength(); index2++) {
					if (nodeEquals(item1, list2.item(index2))) {
						matchSingle = true;
						break;
					} else {
						continue;
					}
				}
				// if this item from list1 does not exist in list2, test fails, there's no
				// need to go further
				if (!matchSingle) {
					return false;
				} else {
					continue;
				}
			}
			return true;
		}
	}
	
	public static boolean evaluateIn(Object object1, Object object2) {
		if ((object1 == null) || (object2 == null)) {
			return false;
		} else if (object1 instanceof Boolean || object2 instanceof Boolean){
			return false;
		} else if (object1 instanceof DOMNodeList && object2 instanceof DOMNodeList){
			IgnoreWhitespaceNodeList noWhitespaceList1 = new IgnoreWhitespaceNodeList((DOMNodeList) object1);
			IgnoreWhitespaceNodeList noWhitespaceList2 = new IgnoreWhitespaceNodeList((DOMNodeList) object2);
			return evaluateIn(noWhitespaceList1, noWhitespaceList2);
		} else if (object1 instanceof String && object2 instanceof String){
			return object1.equals(object2);
		} else if (object1 instanceof String && object2 instanceof DOMNodeList){
			IgnoreWhitespaceNodeList list = new IgnoreWhitespaceNodeList((DOMNodeList) object2);
			String value = (String) object1;
			for (int index = 0; index < list.getLength(); index++){
				Node item = list.item(index);
				if (item.getNodeType() == Node.TEXT_NODE && item.getNodeValue().equals(value)){
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else if (object1 instanceof DOMNodeList && object2 instanceof String) {
			IgnoreWhitespaceNodeList list = new IgnoreWhitespaceNodeList((DOMNodeList) object1);
			Node firstItem = list.item(0);
			String value = (String) object2;
			if (list.getLength() > 1){
				return false;
			} else if (firstItem.getNodeType() != Node.TEXT_NODE){
				return false;
			} else {
				return firstItem.getNodeValue().equals(value);
			}
		} else {
			return false;
		}
	}

	public static boolean nodeEquals(org.w3c.dom.Node node1, org.w3c.dom.Node node2) {
		if ((node1 instanceof Element) && (node2 instanceof Element)) {
			nu.xom.Element element1 = DOMConverter.convert((Element) node1);
			nu.xom.Element element2 = DOMConverter.convert((Element) node2);
			try {
				XOMTestCase.assertEquals(element1, element2);
				return true;
			} catch (AssertionFailedError e) {
				return false;
			}
		} else if ((node1 instanceof Attr) && (node2 instanceof Attr)) {
			Attribute attribute1 = DOMConverter.convert((Attr) node1);
			Attribute attribute2 = DOMConverter.convert((Attr) node2);
			try {
				XOMTestCase.assertEquals(attribute1, attribute2);
				return true;
			} catch (AssertionFailedError e) {
				return false;
			}
		} else if ((node1 instanceof Text) && (node2 instanceof Text)){
			nu.xom.Text text1 = DOMConverter.convert((Text) node1);
			nu.xom.Text text2 = DOMConverter.convert((Text) node2);
			try{
				XOMTestCase.assertEquals(text1, text2);
				return true;
			} catch(AssertionFailedError e){
				return false;
			}
		} else {
			return false;
		}
	}
}
