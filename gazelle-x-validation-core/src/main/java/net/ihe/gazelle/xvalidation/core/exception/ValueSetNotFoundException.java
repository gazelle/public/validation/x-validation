package net.ihe.gazelle.xvalidation.core.exception;

import net.ihe.gazelle.xvalidation.core.model.InValueSet;

public class ValueSetNotFoundException extends GazelleXValidationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2355287017620193882L;

	public ValueSetNotFoundException(InValueSet expression) {
		super(expression.getLocator(), "Value set with id : " + expression.getValueSetId()
				+ " has not been found");
		setNotifyAdministrator(true);
	}
}
