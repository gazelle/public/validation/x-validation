/**
 * DICOMLibrary.java
 *
 */
package net.ihe.gazelle.xvalidation.core.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Description of the enumeration DICOMLibrary.
 *
 */

@XmlType(name = "DICOMLibrary")
@XmlEnum
@XmlRootElement(name = "DICOMLibrary")
public enum DICOMLibrary {
	@XmlEnumValue("dcm4che2")
	DCM4CHE2("dcm4che2"),
	@XmlEnumValue("pixelmed")
	PIXELMED("pixelmed"),
	@XmlEnumValue("NA")
	NA("not applicable");

	private final String value;


	DICOMLibrary(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static DICOMLibrary fromValue(String v) {
		for (DICOMLibrary c : DICOMLibrary.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}