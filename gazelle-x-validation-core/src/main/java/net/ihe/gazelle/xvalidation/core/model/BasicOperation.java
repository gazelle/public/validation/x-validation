/**
 * BasicOperation.java
 *
 * File generated from the XValidator4::BasicOperation uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.xvalidation.core.model;

// End of user code
import java.util.ArrayList;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.exception.NoNodeSelectedException;
import net.ihe.gazelle.xvalidation.core.report.Aborted;
import net.ihe.gazelle.xvalidation.core.report.Notification;
import net.ihe.gazelle.xvalidation.core.report.Report;
import net.ihe.gazelle.xvalidation.core.xpath.XOMUtils;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import net.sf.saxon.dom.DOMNodeList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

/**
 * Description of the class BasicOperation.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicOperation", propOrder = { "locator1", "locator2", "basicOperator" })
@XmlRootElement(name = "BasicOperation")
@Entity
@DiscriminatorValue("basicOperation")
public class BasicOperation extends AbstractExpression implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(BasicOperation.class);

	@XmlElement(name = "Locator1", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "locator_1_id")
	private Locator locator1;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "locator_2_id")
	@XmlElement(name = "Locator2", required = true)
	private Locator locator2;

	@XmlAttribute(name = "basicOperator", required = true)
	@Column(name = "basic_operator")
	private BasicOperatorType basicOperator;

	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	@Transient
	private org.w3c.dom.Node xmlNodePresentation;

	public BasicOperation() {
		super();
	}

	public BasicOperation(BasicOperatorType operator) {
		super();
		this.basicOperator = operator;
		this.locator1 = new Locator();
		this.locator2 = new Locator();
	}
	
	public BasicOperation(BasicOperation operation){
		super();
		this.basicOperator = operation.getBasicOperator();
		this.locator1 = new Locator(operation.getLocator1());
		this.locator2 = new Locator(operation.getLocator2());
	}

	/**
	 * Return locator1.
	 *
	 * @return locator1
	 */
	public Locator getLocator1() {
		return locator1;
	}

	/**
	 * Set a value to attribute locator1.
	 *
	 * @param locator1
	 *            .
	 */
	public void setLocator1(Locator locator1) {
		this.locator1 = locator1;
	}

	/**
	 * Return locator2.
	 *
	 * @return locator2
	 */
	public Locator getLocator2() {
		return locator2;
	}

	/**
	 * Set a value to attribute locator2.
	 *
	 * @param locator2
	 *            .
	 */
	public void setLocator2(Locator locator2) {
		this.locator2 = locator2;
	}

	/**
	 * Return basicOperator.
	 *
	 * @return basicOperator
	 */
	public BasicOperatorType getBasicOperator() {
		return basicOperator;
	}

	/**
	 * Set a value to attribute basicOperator.
	 *
	 * @param basicOperator
	 *            .
	 */
	public void setBasicOperator(BasicOperatorType basicOperator) {
		this.basicOperator = basicOperator;
	}

	@Override
	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "BasicOperation", this);
		}
		return xmlNodePresentation;
	}

	@Override
	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

	@Override
	public Notification evaluate(Map<String, Node> files, ValidatorConfiguration configuration, Level level) {

		Object object1;
		Object object2;
		StringBuilder xpathReport = new StringBuilder();

		try {
			object1 = XpathUtils.evaluateXPath(files.get(locator1.getAppliesOn()), locator1, configuration.getNamespaceContext());
			object2 = XpathUtils.evaluateXPath(files.get(locator2.getAppliesOn()), locator2, configuration.getNamespaceContext());
		} catch (GazelleXValidationException e){
			return abort("Error evaluating XPath : " + e.getMessage(), e.getLocator(), "Error evaluating XPath " + e.getLocator().toString() + " : " + e.getMessage());
		}

		Integer size1 = XpathUtils.getObjectLength(object1);
		if (size1 == null || size1 == 0){
			return abort("XPath did not select anything", locator1, XpathUtils.getXPathReport(locator1, object1));
		}
		Integer size2 = XpathUtils.getObjectLength(object2);
		if (size2 == null || size2 == 0){
			return abort("XPath did not select anything", locator2, XpathUtils.getXPathReport(locator2, object2));
		}

		boolean result;
		switch (this.basicOperator) {
			case CONTAIN:
				result = XOMUtils.evaluateIn(object2, object1);
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "CONTAIN " + XpathUtils.getXPathReport(locator2, object2));
				break;
			case EQUAL:
				result = XOMUtils.evaluateIn(object1, object2) && XOMUtils.evaluateIn(object2, object1);
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "EQUAL " + XpathUtils.getXPathReport(locator2, object2));
				break;
			case IN:
				result = XOMUtils.evaluateIn(object1, object2);
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "IN " + XpathUtils.getXPathReport(locator2, object2));
				break;
			case SIZEEQUAL:
				result = size1 == size2;
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "SIZE EQUAL " + XpathUtils.getXPathReport(locator2, object2));
				break;
			case SIZEGREATEROREQUAL:
				result = size1 >= size2;
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "SIZE >= " + XpathUtils.getXPathReport(locator2, object2));
				break;
			case SIZELESSOREQUAL:
				result = size1 <= size2;
				xpathReport.append(XpathUtils.getXPathReport(locator1, object1) + "SIZE <= " + XpathUtils.getXPathReport(locator2, object2));

				break;
			default:
				String message = this.basicOperator.getValue() + " is not a supported operation";
				log.error(message);
				return abort(message, null, "Unsupported operation : " + this.basicOperator.getValue());
		}

		Notification resultNotification;
		if (!result){
			resultNotification = newNotificationForLevel(level);
		} else {
			resultNotification = newNotificationForLevel(null);
		}
		resultNotification.setXpathReport(xpathReport.toString());
		return resultNotification;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (this.basicOperator.equals(BasicOperatorType.SIZEEQUAL)
				|| this.basicOperator.equals(BasicOperatorType.SIZEGREATEROREQUAL)
				|| this.basicOperator.equals(BasicOperatorType.SIZELESSOREQUAL)) {
			builder.append(" count(");
			builder.append(this.locator1.toString());
			builder.append(')');
			builder.append(this.basicOperator.getNotation());
			builder.append("count(");
			builder.append(this.locator2.toString());
			builder.append(')');
		} else {
			builder.append(this.locator1.toString());
			builder.append(this.basicOperator.getNotation());
			builder.append(this.locator2.toString());
		}
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((basicOperator == null) ? 0 : basicOperator.hashCode());
		result = (prime * result) + ((locator1 == null) ? 0 : locator1.hashCode());
		result = (prime * result) + ((locator2 == null) ? 0 : locator2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BasicOperation other = (BasicOperation) obj;
		if (basicOperator != other.basicOperator) {
			return false;
		}
		if (locator1 == null) {
			if (other.locator1 != null) {
				return false;
			}
		} else if (!locator1.equals(other.locator1)) {
			return false;
		}
		if (locator2 == null) {
			if (other.locator2 != null) {
				return false;
			}
		} else if (!locator2.equals(other.locator2)) {
			return false;
		}
		return true;
	}

	@Override
	public String getEditLink() {
		return "/xvalidation/expressions/basicOperation.xhtml";
	}

	@Override
	public void testCorrectness() throws AssertionFailedError {
		Assert.assertNotNull("[Expression #" + getFakeId() + "] first member is null", locator1);
		Assert.assertTrue("[Expression #" + getFakeId() + "] first member is not correctly filled out",
				locator1.isValued());
		Assert.assertNotNull("[Expression #" + getFakeId() + "] second member is null", locator2);
		Assert.assertTrue("[Expression #" + getFakeId() + "] second member is not correctly filled out",
				locator2.isValued());
		Assert.assertNotNull("[Expression #" + getFakeId() + "] no operator defined" + basicOperator);
	}

	@Override
	public ArrayList<String> appliesTo() throws AssertionFailedError {
		testCorrectness();
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(locator1.getAppliesOn());
		keywords.add(locator2.getAppliesOn());
		return keywords;
	}

	@Override
	public AbstractExpression createCopy() {
		return new BasicOperation(this);
	}
}