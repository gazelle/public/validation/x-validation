package net.ihe.gazelle.xvalidation.core.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.w3c.dom.Node;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Error")
public class Error extends Notification {

	/**
	 *
	 */
	private static final long serialVersionUID = 1653624722196562383L;

	@XmlTransient
	private Node xmlNodePresentation;

	@Override
	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "Error", this);
		}
		return xmlNodePresentation;
	}

	@Override
	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

}
