package net.ihe.gazelle.xvalidation.core.utils;

import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.report.XSDValidation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ExpressionConverter {

	private static Logger log = LoggerFactory.getLogger(ExpressionConverter.class);

	private ExpressionConverter() {

	}

	/**
	 * Marshall the Gazelle Cross Validator and convert each rule in a readable string
	 * 
	 * @param args
	 */
	public static void main(String[] args) { // throws JAXBException, IOException
		try {
			if (args.length > 0) {
				String filename = args[0];
				XSDValidation report = GazelleCrossValidatorType.validateXmlFile(filename);
				if (!report.validationPassed()) {
					log.error("The provided X Validator file is not valid, see details below");
					report.print(log);
				} else {
					GazelleCrossValidatorType validator = GazelleCrossValidatorTransformer.toObject(new FileInputStream(
							filename));
					log.info(convertExpressionsFromValidator(validator));
				}
			} else {
				log.error("Usage: gazelle-x-validation-expression-converter validatorFile");
			}
		} catch (JAXBException j) {
			log.error("JAXBException in ExpressionConverter main method" + j.getCause());
		} catch (IOException i) {
		log.error("IOException in ExpressionConverter main method" + i.getMessage());
	}
	}

	public static String convertExpressionsFromValidator(GazelleCrossValidatorType validator) {
		StringBuilder result = new StringBuilder();
		if ((validator.getConfiguration() != null) && !validator.getConfiguration().getNamespaces().isEmpty()) {
			result.append("\n---- Namespaces ----\n");
			for (Namespace namespace : validator.getConfiguration().getNamespaces()) {
				result.append(namespace.getPrefix() + " : " + namespace.getUri() + "\n");
			}
		}
		if (!validator.getRules().isEmpty()) {
			result.append("\n---- Rules ----\n");
			for (Rule rule : validator.getRules()) {
				result.append(rule.getKeyword() + " : " + rule.getExpression().getExpression().toString() + "\n\n");
			}
		} else {
			log.warn("No rule enclosed in this validator");
		}
		return result.toString();
	}

}
