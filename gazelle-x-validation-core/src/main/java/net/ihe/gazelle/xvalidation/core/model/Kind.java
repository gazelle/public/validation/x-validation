/**
 * Kind.java
 *
 * File generated from the XValidator4::Kind uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.xvalidation.core.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.hql.FilterLabel;

/**
 * Description of the enumeration Kind.
 *
 *
 */

@XmlType(name = "Kind")
@XmlEnum
@XmlRootElement(name = "Kind")
public enum Kind {
	@XmlEnumValue("CDA")
	CDA("CDA"),
	@XmlEnumValue("HL7v2")
	HL7V2("HL7v2"),
	@XmlEnumValue("XML")
	XML("XML"),
	@XmlEnumValue("DICOM")
	DICOM("DICOM"),
	@XmlEnumValue("REST")
	REST("REST");

	private final String value;

	Kind(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	@FilterLabel
	public String getValue() {
		return value;
	}

	public static Kind fromValue(String v) {
		for (Kind c : Kind.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}