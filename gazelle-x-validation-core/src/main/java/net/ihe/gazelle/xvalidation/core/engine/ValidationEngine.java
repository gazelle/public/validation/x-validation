package net.ihe.gazelle.xvalidation.core.engine;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.report.Error;
import net.ihe.gazelle.xvalidation.core.report.*;
import net.ihe.gazelle.xvalidation.core.utils.GazelleCrossValidatorReportTransformer;
import net.ihe.gazelle.xvalidation.core.utils.XMLValidation;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.*;

public class ValidationEngine {

    private static Logger log = LoggerFactory.getLogger(ValidationEngine.class);

    /**
     * @param validator
     * @param files     a map containing the paths to the files to validate
     * @throws IOException
     * @throws GazelleXValidationException
     */
    public ValidationEngine(GazelleCrossValidatorType validator, Map<String, Object> files)
            throws GazelleXValidationException, IOException {
        this.validator = validator;
        notifications = new ArrayList<Notification>();
        createNodesFromFiles(files);
    }

    private List<Notification> notifications;
    private GazelleCrossValidatorType validator = null;
    private Map<String, Node> nodes;
    private Map<String, String> filenames;
    private GazelleCrossValidationReportType report;
    private Counters counters;

    public void validate() throws GazelleXValidationException {
        if (validator == null) {
            throw new GazelleXValidationException("No GazelleCrossValidator set");
        }
        report = new GazelleCrossValidationReportType();
        Summary summary = new Summary();
        GazelleCrossValidatorDescription description = new GazelleCrossValidatorDescription();
        description.setEngineVersion(getJarVersion());
        description.setCalledValidator(new CrossValidatorDescription(validator.getName(), validator.getVersion(),
                validator.getAffinityDomain()));
        description.setAssertionManagerUrl(validator.getAssertionManagerUrl());
        summary.setGazelleXValidator(description);
        summary.setValidationDate(new Date());
        // XML / XSD Validation
        for (ValidatorInput input : validator.getValidatedObjects()) {
            if (input.requiresXSDValidation()) {
                try {
                    report.addXsdValidation(validateAgainstXsd(input.getKeyword(), input.getXsdLocation()));
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }
        // TODO take the parentvalidator into account
        counters = new Counters();
        List<Rule> rules = validator.getRules();
        List<Rule> additionalRules = getAdditionalRulesToExecute();
        //TODO rework getAdditionalRulesToExecute not to return null and directly add them to rules + change name to understand its rules from parent
        if (additionalRules != null) {
            rules.addAll(additionalRules);
        }
        for (Rule rule : rules) {
            if (rule.isActive() && allFilesAreAvailableForExecutingRule(rule.getAppliesTo())) {
                evaluateRule(rule);
            }
            // TODO do we report something if a rule is inactive or deprecated ???
        }
        report.setNotifications(new NotificationsType(notifications));
        counters.setNrOfChecks(notifications.size());
        summary.setValidationCounters(counters);
        // take the result of the XSD validation into account
        boolean passed = true;
        for (XSDValidation validation : report.getXsdValidations()) {
            if (!validation.validationPassed()) {
                passed = false;
                break;
            }
        }
        if (passed && counters.getNrOfValidationErrors() == 0 && counters.getNrOfValidationAborted() == 0) {
            summary.setValidationResult(ValidationResultType.PASSED);
        } else if (counters.getNrOfValidationAborted() != 0) {
            summary.setValidationResult(ValidationResultType.DONE_UNDEFINED);
        } else {
            summary.setValidationResult(ValidationResultType.FAILED);
        }
        report.setReportSummary(summary);
    }

    /**
     * This method retrieves the inherited rules, it can be overridden when instanciated the class
     *
     * @return
     */
    protected List<Rule> getAdditionalRulesToExecute() {
        // TODO how do we handle this case when called from the command-line
        return null;
    }

    private String getJarVersion() {
        Properties properties = new Properties();
        String validationEngineVersion = null;
        try {
            InputStream is = ValidationEngine.class.getResourceAsStream("/version.properties");
            if (is != null) {
                properties.load(is);
                validationEngineVersion = properties.getProperty("version");
            } else {
                log.warn("cannot read properties from version.properties");
            }
        } catch (IOException e) {
            log.error("cannot load version.properties file");
        }
        return validationEngineVersion;
    }

    public String getReportAsString() {
        return GazelleCrossValidatorReportTransformer.toXml(report);
    }

    public void saveReport(String path) {
        if (report != null) {
            try {
                FileOutputStream fos = new FileOutputStream(path);
                GazelleCrossValidatorReportTransformer.toXml(fos, report);
                fos.close();
            } catch (FileNotFoundException e) {
                log.error(e.getMessage(), e);
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * @param rule
     * @return
     */
    public void evaluateRule(Rule rule) {
        if (notifications == null) {
            this.notifications = new ArrayList<Notification>();
        }
        Notification ruleNotification;
        ruleNotification = rule.evaluate(nodes, validator.getConfiguration());
        notifications.add(ruleNotification);
        incrementCounter(ruleNotification);
    }

    private void incrementCounter(Notification notification) {
        if (notification instanceof Report) {
            counters.incrementReport();
        } else if (notification instanceof Error) {
            counters.incrementError();
        } else if (notification instanceof Info) {
            counters.incrementInfo();
        } else if (notification instanceof Warning) {
            counters.incrementWarning();
        } else if (notification instanceof Aborted) {
            counters.incrementAborted();
        } else {
            counters.incrementUnknown();
        }
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setValidator(GazelleCrossValidatorType validator) {
        this.validator = validator;
    }

    public void setNodes(Map<String, Node> filesAsNodes) {
        this.nodes = filesAsNodes;
    }

    public void createNodesFromFiles(Map<String, Object> objects) throws GazelleXValidationException, IOException {
        if (objects != null) {
            nodes = new HashMap<String, Node>();
            filenames = new HashMap<String, String>();
            for (Map.Entry<String, Object> entry : objects.entrySet()) {
                String key = entry.getKey();
                Object value = objects.get(key);
                if (value instanceof Document) {
                    nodes.put(key, (Document) value);
                } else if (value instanceof String) {
                    String filename = (String) value;
                    filenames.put(key, filename);
                    FileInputStream fis = new FileInputStream(filename);
                    nodes.put(key, XpathUtils.parse(fis));
                    fis.close();
                }
            }
        }
    }

    private XSDValidation validateAgainstXsd(String keyword, String xsdLocation) throws GazelleXValidationException,
            IOException {
        String filename = filenames.get(keyword);
        if (filename == null) {
            throw new GazelleXValidationException("No file provided for keyword " + keyword);
        } else {
            File fileToValidate = new File(filename);
            XSDValidation result = XMLValidation.isXMLWellFormedAndValid(fileToValidate, xsdLocation);
            result.setKeyword(keyword);
            result.setXsdLocation(xsdLocation);
            result.setFile(filename);
            return result;
        }
    }

    private boolean allFilesAreAvailableForExecutingRule(List<String> keywords) {
        for (String keyword : keywords) {
            if (!nodes.containsKey(keyword)) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    public ValidationResultType getStatus() {
        if (report != null) {
            return report.getValidationResult();
        } else {
            return null;
        }
    }

    public GazelleCrossValidatorType getValidator() {
        return validator;
    }

}
