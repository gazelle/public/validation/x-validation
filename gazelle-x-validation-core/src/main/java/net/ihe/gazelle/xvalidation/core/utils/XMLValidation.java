package net.ihe.gazelle.xvalidation.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;
import net.ihe.gazelle.xvalidation.core.report.ValidationResultType;
import net.ihe.gazelle.xvalidation.core.report.XSDDetail;
import net.ihe.gazelle.xvalidation.core.report.XSDValidation;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public final class XMLValidation {

	private static Logger log = LoggerFactory.getLogger(XMLValidation.class);

	private XMLValidation() {

	}

	private static Map<String, SAXParserFactory> factories;
	private static SAXParserFactory basicFactory;
	private static final String XVALIDATOR_XSD = "/XValidator.xsd";

	static {
		try {
			basicFactory = SAXParserFactory.newInstance();
			basicFactory.setValidating(false);
			basicFactory.setNamespaceAware(true);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		factories = new HashMap<String, SAXParserFactory>();
	}

	private static SAXParserFactory getFactory(String xsdLocation) throws SAXException {
		synchronized (factories) {
			if (factories.containsKey(xsdLocation)) {
				return factories.get(xsdLocation);
			} else {
				SAXParserFactory factory = initFactoryFromSchema(xsdLocation);
				factories.put(xsdLocation, factory);
				return factory;
			}
		}
	}

	private static SAXParserFactory initFactoryFromSchema(String xsdpath) throws SAXException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setXIncludeAware(true);
		factory.setNamespaceAware(true);
		SchemaFactory sfactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		sfactory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
		StreamSource source;
		if (xsdpath.equals(XVALIDATOR_XSD)) {
			InputStream is = XMLValidation.class.getResourceAsStream(XVALIDATOR_XSD);
			source = new StreamSource(is);
		} else {
			source = new StreamSource(xsdpath);
		}
		Schema schema = sfactory.newSchema(source);
		factory.setSchema(schema);
		return factory;
	}

	public static XSDValidation isXMLWellFormedAndValid(File file, String xsdLocation) throws IOException {
		XSDValidation validationResult = new XSDValidation();
		validationResult.setXsdLocation(xsdLocation);
		// first check that the document is well-formed
		FileInputStream fis = new FileInputStream(file);
		if (isXMLWellFormed(fis, validationResult) && xsdLocation != null) {
			FileInputStream fis2 = new FileInputStream(file);
			validateAgainstXSD(fis2, validationResult, xsdLocation);
			fis2.close();
		}
		fis.close();
		return validationResult;
	}

	private static boolean isXMLWellFormed(InputStream inputStream, XSDValidation results) {
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			synchronized (basicFactory) {
				XSDValidator xsdValidator = new XSDValidator();
				exceptions = xsdValidator.validateUsingFactoryAndSchema(inputStream, null, basicFactory);
			}
		} catch (Exception e) {
			exceptions.add(handleException(e));
		}
		extractValidationResult(exceptions, results);
		return results.getResult().equals(ValidationResultType.PASSED);
	}

	private static synchronized void validateAgainstXSD(InputStream inputStream, XSDValidation result,
			String xsdLocation) {

		try {
			SAXParserFactory factory = getFactory(xsdLocation);
			List<ValidationException> exceptions = new ArrayList<ValidationException>();
			try {
				synchronized (factory) {
					XSDValidator xsdValidator = new XSDValidator();
					exceptions = xsdValidator.validateUsingFactoryAndSchema(inputStream, xsdLocation, factory);
				}
			} catch (Exception e) {
				result.addDetail(new XSDDetail(e.getMessage()));
				result.setResult(ValidationResultType.FAILED);
				log.error(e.getMessage(), e);
			}
			extractValidationResult(exceptions, result);
		} catch (SAXException e1) {
			result.addDetail(new XSDDetail(e1.getMessage()));
			result.setResult(ValidationResultType.ABORTED);
			log.error("An error occured when building factory: " + e1.getMessage(), e1);
		}
	}

	private static void extractValidationResult(List<ValidationException> exceptions, XSDValidation result) {
		if (exceptions == null || exceptions.isEmpty()) {
			result.setResult(ValidationResultType.PASSED);
		} else {
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			for (ValidationException ve : exceptions) {
				if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))) {
					nbOfWarnings++;
				} else {
					nbOfErrors++;
				}
				XSDDetail xsd = new XSDDetail();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (StringUtils.isNumeric(ve.getLineNumber())) {
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (StringUtils.isNumeric(ve.getColumnNumber())) {
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				result.addDetail(xsd);
			}
			result.setErrors(nbOfErrors);
			result.setWarnings(nbOfWarnings);
			if (nbOfErrors > 0) {
				result.setResult(ValidationResultType.FAILED);
			} else {
				result.setResult(ValidationResultType.PASSED);
			}
		}

	}

	private static ValidationException handleException(Exception e) {
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null) {
			ve.setMessage("error on validating : " + e.getMessage());
		} else if (e != null && e.getCause() != null && e.getCause().getMessage() != null) {
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		} else {
			ve.setMessage("error on validating. The exception generated is of kind : " + e.getClass().getSimpleName());
		}
		ve.setSeverity("error");
		return ve;
	}

	public static XSDValidation validateXValidatorFile(File validatorFile) throws IOException {
		return isXMLWellFormedAndValid(validatorFile, XVALIDATOR_XSD);
	}

}
