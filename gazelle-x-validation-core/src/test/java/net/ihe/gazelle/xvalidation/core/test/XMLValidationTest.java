package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.report.ValidationResultType;
import net.ihe.gazelle.xvalidation.core.report.XSDValidation;
import net.ihe.gazelle.xvalidation.core.utils.XMLValidation;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class XMLValidationTest {

	@Test
	public void testWithXSDInResources() throws IOException {
		File file = new File("src/test/resources/ValidatorSample.xml");
		XSDValidation result = XMLValidation.validateXValidatorFile(file);
		Assert.assertEquals("Result of the validation is not the one expected", ValidationResultType.PASSED,
				result.getResult());
	}

	@Test
	public void testWithXSDOnDisc() throws IOException {
		File file = new File("src/test/resources/ValidatorSample.xml");
		String xsd = "src/main/resources/XValidator.xsd";
		XSDValidation result = XMLValidation.isXMLWellFormedAndValid(file, xsd);
		Assert.assertEquals("Result of the validation is not the one expected", ValidationResultType.PASSED,
				result.getResult());
	}
	
	@Test
	public void testWellFormedXML() throws IOException {
		File file = new File("src/test/resources/ValidatorSample.xml");
		XSDValidation result = XMLValidation.isXMLWellFormedAndValid(file, null);
		Assert.assertEquals("Result of validation is not the one expected", ValidationResultType.PASSED,
				result.getResult());
	}

	@Test
	public void testNotWellFormedXml() throws IOException {
		File file = new File("src/test/resources/NotAWellFormedXMLFile.xml");
		XSDValidation result = XMLValidation.isXMLWellFormedAndValid(file, null);
		Assert.assertEquals("Result of validation is not the one expected", ValidationResultType.FAILED,
				result.getResult());
	}

	@Test
	public void testNonValidXml() throws IOException {
		File file = new File("src/test/resources/NotAValidValidatorSample.xml");
		XSDValidation result = XMLValidation.validateXValidatorFile(file);
		Assert.assertEquals("Result of the validation is not the one expected", ValidationResultType.FAILED,
				result.getResult());
	}

	//FIXME Problem with xsd
	@Ignore
	@Test
	public void testWithXSDOnInternet() throws IOException {
		File file = new File("src/test/resources/ValidatorSample.xml");
		String xsd = "https://gazelle.ihe.net/jenkins/job/gazelle-x-validation/ws/gazelle-x-validation-core/src/main/resources/XValidator.xsd";
		XSDValidation result = XMLValidation.isXMLWellFormedAndValid(file, xsd);
		Assert.assertEquals("Result of the validation is not the one expected", ValidationResultType.PASSED,
				result.getResult());
	}
}
