/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.report.*;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DateComparisonTest {

    private static Locator locator1;
    private static Locator locator2;
    private static Locator locator3;
    private static Locator locator4;
    private static final String DOC_1 = "doc1";
    private static final String DOC_2 = "doc2";
    private static final String DOC_3 = "doc3";
    private static final String DOC_4 = "doc4";
    private static final DateFormatType TO_THE_SECOND = new DateFormatType("TO_THE_SECOND", "yyyyMMddHHmmss", 9);
    private static final DateFormatType TO_THE_DAY = new DateFormatType("TO_THE_DAY", "yyyyMMdd", 7);
    private static final DateFormatType TO_THE_MINUTE = new DateFormatType("TO_THE_MINUTE", "yyyyMMddHHmm", 8);
    private static final DateFormatType WITH_TIMEZONE = new DateFormatType("WITH_TIMEZONE", "yyyyMMddHHmmssZZZZZ", 10);
    private static HashMap<String, Node> files;
    private static ValidatorConfiguration validatorConfig;

    @BeforeClass
    public static void initClass() {

        locator1 = new Locator(DOC_1, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        locator2 = new Locator(DOC_2, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        locator3 = new Locator(DOC_3, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        locator4 = new Locator(DOC_4, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        files = new HashMap<String, Node>();
        InputStream doc1File = null;
        InputStream doc2File = null;
        InputStream doc3File = null;
        InputStream doc4File = null;
        try {
            doc1File = new FileInputStream("src/test/resources/datecomparison/doc1.xml");
            doc2File = new FileInputStream("src/test/resources/datecomparison/doc2.xml");
            doc3File = new FileInputStream("src/test/resources/datecomparison/doc3.xml");
            doc4File = new FileInputStream("src/test/resources/datecomparison/doc4.xml");
            files.put(
                    DOC_1,
                    XpathUtils.parse(doc1File));
            files.put(
                    DOC_2,
                    XpathUtils.parse(doc2File));
            files.put(
                    DOC_3,
                    XpathUtils.parse(doc3File));
            files.put(
                    DOC_4,
                    XpathUtils.parse(doc4File));
            List<Namespace> namespaces = new ArrayList<Namespace>();
            namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
            validatorConfig = new ValidatorConfiguration();
            validatorConfig.setNamespaces(namespaces);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(doc1File);
            IOUtils.closeQuietly(doc2File);
            IOUtils.closeQuietly(doc3File);
            IOUtils.closeQuietly(doc4File);
        }
    }

    @Test
    public void testEqualsTrueDifferentFormat() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locator1);
        comp.setLocator2(locator2);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n" +
                "= [doc2::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221170527+0100\"]\n", result.getXpathReport());
        Assert.assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void testEqualsTrueSameFormat() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locator1);
        comp.setLocator2(locator1);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.INFO);
        Assert.assertEquals("[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n" +
                "= [doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n", result.getXpathReport());
        Assert.assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void testEqualsFalse() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locator1);
        comp.setLocator2(locator3);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n" +
                "= [doc3::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190121170527+0100\"]\n", result.getXpathReport());
        Assert.assertTrue("Test failed", result instanceof Warning);
    }

    @Test
    public void testFormatNotAvailable() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locator1);
        comp.setLocator2(locator3);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification notification = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("Cannot match date from locator 1 to one of given date format : " +
                "[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n", notification.getXpathReport());
        Assert.assertEquals("Cannot match date from locator 1 to one of given date format.", ((Aborted) notification).getReason());
    }

    @Test
    public void testNodeNotAvailable() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locator1);
        comp.setLocator2(locator4);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification notification = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("Error evaluating XPath doc4::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value : This XPath has not returned any element", notification.getXpathReport());
        Assert.assertEquals("Error evaluating XPath : This XPath has not returned any element", ((Aborted) notification).getReason());
    }

    @Test
    public void testBeforeTrue() {
        DateComparison comp = new DateComparison(DateOperatorType.BEFORE);
        comp.setLocator1(locator3);
        comp.setLocator2(locator1);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[doc3::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190121170527+0100\"]\n" +
                "< [doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n", result.getXpathReport());
        Assert.assertTrue("test failed", result instanceof Report);
    }

    @Test
    public void testBeforeFalse() {
        DateComparison comp = new DateComparison(DateOperatorType.BEFORE);
        comp.setLocator1(locator1);
        comp.setLocator2(locator3);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.INFO);
        Assert.assertEquals("[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n" +
                "< [doc3::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190121170527+0100\"]\n", result.getXpathReport());
        Assert.assertTrue("test failed", result instanceof Info);
    }

    @Test
    public void testAfterTrue() {
        DateComparison comp = new DateComparison(DateOperatorType.AFTER);
        comp.setLocator1(locator1);
        comp.setLocator2(locator3);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n" +
                "> [doc3::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190121170527+0100\"]\n", result.getXpathReport());
        Assert.assertTrue("test failed", result instanceof Report);
    }

    @Test
    public void testAfterFalse() {
        DateComparison comp = new DateComparison(DateOperatorType.AFTER);
        comp.setLocator1(locator3);
        comp.setLocator2(locator1);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_DAY);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        System.out.println(comp.toString());
        Notification result = comp.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[doc3::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190121170527+0100\"]\n" +
                "> [doc1::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value \"20190221160527\"]\n", result.getXpathReport());
        Assert.assertTrue("test failed", result instanceof Warning);
    }

    @Test
    public void toXml(){
        try {

            DateComparison comp = new DateComparison(DateOperatorType.AFTER);
            comp.setLocator1(locator3);
            comp.setLocator2(locator1);

            List<DateFormatType> dateFormatTypeList = new ArrayList<>();
            dateFormatTypeList.add(TO_THE_SECOND);
            dateFormatTypeList.add(TO_THE_DAY);
            dateFormatTypeList.add(TO_THE_MINUTE);
            dateFormatTypeList.add(WITH_TIMEZONE);
            comp.setDateFormats1(dateFormatTypeList);
            comp.setDateFormats2(dateFormatTypeList);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            String xvalidatorXsl = "http://localhost/toto.xsl";
            String xslDeclaration = "<?xml-stylesheet type=\"text/xsl\" href=\"" + xvalidatorXsl + "\" ?>";

            JAXBContext jc = JAXBContext.newInstance(DateComparison.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty("com.sun.xml.bind.xmlHeaders", xslDeclaration);
            m.marshal(comp, byteArrayOutputStream);
            Assert.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?xml-stylesheet type=\"text/xsl\" " +
                    "href=\"http://localhost/toto.xsl\" ?>\n" +
                    "<DateComparison dateOperator=\"after\">\n" +
                    "    <Locator1 appliesOn=\"doc3\" path=\"/*[name()='TestDocument']/*[name()='TestDateComparison']/@value\"/>\n" +
                    "    <DateFormats1>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmmss\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMdd\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmm\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmmssZZZZZ\"/>\n" +
                    "    </DateFormats1>\n" +
                    "    <Locator2 appliesOn=\"doc1\" path=\"/*[name()='TestDocument']/*[name()='TestDateComparison']/@value\"/>\n" +
                    "    <DateFormats2>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmmss\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMdd\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmm\"/>\n" +
                    "        <DateFormatType value=\"yyyyMMddHHmmssZZZZZ\"/>\n" +
                    "    </DateFormats2>\n" +
                    "</DateComparison>\n", byteArrayOutputStream.toString("UTF-8"));
        } catch (Exception e){
            System.out.println(e);
            Assert.fail(e.getMessage());
        }

    }
}
