/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.test;

import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.report.*;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class DateComparisonAbortCasesTest {

    private static Locator locatorInvalidFormat;
    private static Locator locatorNotMatchingFormat;
    private static Locator locatorBadXPath;
    private static Locator locatorValidFormatWithFrenchTimezone;

    private static final String INVALID_FORMAT = "invalidFormat";
    private static final String NOT_MATCHING_FORMAT = "notMatchingFormat";
    private static final String VALID_FORMAT_WITH_FRENCH_TIMEZONE = "validFormatWithFrenchTimezone";

    private static final DateFormatType WITH_TIMEZONE = new DateFormatType("WITH_TIMEZONE", "yyyyMMddHHmmssZ", 10);
    private static final DateFormatType TO_THE_SECOND = new DateFormatType("TO_THE_SECOND", "yyyyMMddHHmmss", 9);
    private static final DateFormatType TO_THE_MINUTE = new DateFormatType("TO_THE_MINUTE", "yyyyMMddHHmm", 8);
    private static final DateFormatType TO_THE_DAY = new DateFormatType("TO_THE_DAY", "yyyyMMdd", 7);

    private static HashMap<String, Node> files;
    private static ValidatorConfiguration validatorConfig;

    @BeforeClass
    public static void initClass() {

        locatorInvalidFormat = new Locator(INVALID_FORMAT, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        locatorNotMatchingFormat = new Locator(NOT_MATCHING_FORMAT, "/*[name()='TestDocument']/*[name()='TestDateComparison']/@value");
        locatorBadXPath = new Locator(VALID_FORMAT_WITH_FRENCH_TIMEZONE, "invalidXPath///////****//////");
        locatorValidFormatWithFrenchTimezone = new Locator(VALID_FORMAT_WITH_FRENCH_TIMEZONE, "/*[name()='TestDocument']/*[name()='DifferentTime']/@value");
        files = new HashMap<>();
        InputStream invalidFormatFile = null;
        InputStream notMatchingFormatFile = null;
        InputStream validFormatWithFrenchTimezoneFile = null;
        try {
            invalidFormatFile = new FileInputStream("src/test/resources/datecomparison/invalidFormat.xml");
            notMatchingFormatFile = new FileInputStream("src/test/resources/datecomparison/notMatchingFormat.xml");
            validFormatWithFrenchTimezoneFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithFrenchTimezone.xml");
            files.put(INVALID_FORMAT, XpathUtils.parse(invalidFormatFile));
            files.put(NOT_MATCHING_FORMAT, XpathUtils.parse(notMatchingFormatFile));
            files.put(VALID_FORMAT_WITH_FRENCH_TIMEZONE, XpathUtils.parse(validFormatWithFrenchTimezoneFile));

            List<Namespace> namespaces = new ArrayList<>();
            namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
            validatorConfig = new ValidatorConfiguration();
            validatorConfig.setNamespaces(namespaces);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(invalidFormatFile);
            IOUtils.closeQuietly(notMatchingFormatFile);
            IOUtils.closeQuietly(validFormatWithFrenchTimezoneFile);
        }
    }

    @Test
    public void firstXPathIsNotCorrectTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorBadXPath);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Error evaluating XPath validFormatWithFrenchTimezone::invalidXPath///////****////// : " +
                "javax.xml.xpath.XPathExpressionException: net.sf.saxon.trans.XPathException: " +
                "Unexpected token \"//\" at start of expression", result.getXpathReport());
        assertEquals("Error evaluating XPath : javax.xml.xpath.XPathExpressionException: " +
                        "net.sf.saxon.trans.XPathException: Unexpected token \"//\" at start of expression",
                ((Aborted) result).getReason());
    }

    @Test
    public void secondXPathIsNotCorrectTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorBadXPath);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Error evaluating XPath validFormatWithFrenchTimezone::invalidXPath///////****////// : " +
                "javax.xml.xpath.XPathExpressionException: net.sf.saxon.trans.XPathException: " +
                "Unexpected token \"//\" at start of expression", result.getXpathReport());
        assertEquals("Error evaluating XPath : javax.xml.xpath.XPathExpressionException: " +
                        "net.sf.saxon.trans.XPathException: Unexpected token \"//\" at start of expression",
                ((Aborted) result).getReason());
    }

    @Test
    public void firstLocatorDoesNotHaveAValidFormatTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorInvalidFormat);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(TO_THE_DAY);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Cannot match date from locator 1 to one of given date format : " +
                "[invalidFormat::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value " +
                "\"A0202020202020+0200\"]\n", result.getXpathReport());
        assertEquals("Cannot match date from locator 1 to one of given date format.", ((Aborted) result).getReason());
    }

    @Test
    public void secondLocatorDoesNotHaveAValidFormatTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorInvalidFormat);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        dateFormatTypeList.add(TO_THE_SECOND);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(TO_THE_DAY);
        comp.setDateFormats1(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Cannot match date from locator 2 to one of given date format : " +
                "[invalidFormat::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value " +
                "\"A0202020202020+0200\"]\n", result.getXpathReport());
        assertEquals("Cannot match date from locator 2 to one of given date format.", ((Aborted) result).getReason());
    }

    @Test
    public void firstLocatorDoesNotMatchWithGivenFormatsTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorNotMatchingFormat);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Cannot match date from locator 1 to one of given date format : " +
                "[notMatchingFormat::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value " +
                "\"20121221120000\"]\n", result.getXpathReport());
        assertEquals("Cannot match date from locator 1 to one of given date format.", ((Aborted) result).getReason());
    }

    @Test
    public void secondLocatorDoesNotMatchWithGivenFormatsTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorNotMatchingFormat);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        dateFormatTypeList.add(TO_THE_MINUTE);
        dateFormatTypeList.add(TO_THE_DAY);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("Cannot match date from locator 2 to one of given date format : " +
                "[notMatchingFormat::/*[name()='TestDocument']/*[name()='TestDateComparison']/@value matched String with value " +
                "\"20121221120000\"]\n", result.getXpathReport());
        assertEquals("Cannot match date from locator 2 to one of given date format.", ((Aborted) result).getReason());
    }
}
