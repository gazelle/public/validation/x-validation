package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.exception.NoNodeSelectedException;
import net.ihe.gazelle.xvalidation.core.model.Locator;
import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.xpath.ValidatorNamespaceContext;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import net.sf.saxon.dom.DOMNodeList;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.NamespaceContext;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XpathUtilsTest {

    private static FileInputStream document;
    private static NamespaceContext nsContext;
    private static final String FILE = "FILE";

    @BeforeClass
    public static void initClass() {
        List<Namespace> namespaces = new ArrayList<Namespace>();
        namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
        namespaces.add(new Namespace("titi", "urn:hl7-org:v3"));
        namespaces.add(new Namespace("fn", "http://www.w3.org/2005/xpath-functions"));
        nsContext = new ValidatorNamespaceContext(namespaces);
    }

    @Before
    public void initTest() throws IOException {
        document = new FileInputStream("src/test/resources/sampleQuery.xml");
    }

    @After
    public void determinate() throws IOException {
        document.close();
    }

    @Test
    public void testGetNodesFromDOM() throws FileNotFoundException, GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "v3:PRPA_IN201305UV02/v3:controlActProcess");
        DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
        Assert.assertTrue("No node selected with XPath: " + location, (nodes != null && nodes.getLength() > 0));
    }

    @Test
    public void testGetBooleanValue() throws FileNotFoundException, GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "//v3:id/@extension = '35423'");
        Boolean result = XpathUtils.getBooleanValue(documentDOM, location, nsContext);
        Assert.assertTrue("result is not 'true'", result);
    }

    @Test
    public void testGetBooleanValueFalse() throws FileNotFoundException, GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "//v3:id/@extension = '789542125'");
        Boolean result = XpathUtils.getBooleanValue(documentDOM, location, nsContext);
        Assert.assertFalse("result is not 'true'", result);
    }

    @Test
    public void testGetStringValue() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "v3:PRPA_IN201305UV02/v3:id/@extension");
        String result = XpathUtils.getStringValue(documentDOM, location, nsContext);
        Assert.assertTrue("Wrong string value: " + result, result.equals("35423"));
    }

    @Test
    public void testGetBooleanValue2() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "count(v3:PRPA_IN201305UV02/v3:id) eq 1");
        Assert.assertTrue("Wrong result", XpathUtils.getBooleanValue(documentDOM, location, nsContext));
    }

    @Test
    public void testRetrieveElement() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "//v3:id[@extension='35423']");
        DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
        Assert.assertTrue("retrieved node is not an element", nodes.item(0) instanceof Element);
        Assert.assertFalse("retrieved node is an attribute", nodes.item(0) instanceof Attr);
    }

    @Test
    public void testRetrieveAttribute() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "/v3:PRPA_IN201305UV02/v3:id/@root");
        DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
        Assert.assertTrue("No node selected", nodes != null && nodes.getLength() > 0);
        Assert.assertTrue("retrieve node is not an attribute", nodes.item(0) instanceof Attr);
        Assert.assertFalse("retrieve node is an element", nodes.item(0) instanceof Element);
    }

    @Test
    public void testRetrieveDocument() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "/v3:PRPA_IN201305UV02");
        DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
        Assert.assertTrue("No node selected", nodes != null && nodes.getLength() > 0);
        Assert.assertTrue("retrieve node is not an element", nodes.item(0) instanceof Element);
        Assert.assertFalse("retrieve node is an attribute", nodes.item(0) instanceof Attr);
    }

    @Test
    public void testGetStringValueWithoutNamespace() throws GazelleXValidationException, FileNotFoundException {
        FileInputStream fis = new FileInputStream("src/test/resources/dicomWorklist.xml");
        Document documentDOM = XpathUtils.parse(fis);
        Locator location = new Locator(FILE, "/DicomObject/ImplementationVersionName/value/text()");
        String result = XpathUtils.getStringValue(documentDOM, location, nsContext);
        Assert.assertEquals("result equals: " + result, "OFFIS_DCMTK_360", result);
    }

    @Test
    public void testGetStringValueWithDifferentPrefix() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator location = new Locator(FILE, "titi:PRPA_IN201305UV02/titi:id/@extension");
        String result = XpathUtils.getStringValue(documentDOM, location, nsContext);
        Assert.assertTrue("Wrong string value: " + result, result.equals("35423"));
    }

    @Test
    public void testGetStringValueWithoutNamespacePrefixInDoc() throws GazelleXValidationException,
            FileNotFoundException {
        FileInputStream fis = new FileInputStream("src/test/resources/integration/PRE_2014.xml");
        Document documentDOM = XpathUtils.parse(fis);
        Locator location = new Locator(FILE, "/v3:ClinicalDocument/v3:title");
        String result = XpathUtils.getStringValue(documentDOM, location, nsContext);
        Assert.assertEquals("result equals: " + result, "Pharmacy Prescription", result);
    }

    @Test
    public void testGetStringValueWithoutNamespacePrefixInPath() throws FileNotFoundException, GazelleXValidationException {
        FileInputStream fis = new FileInputStream("src/test/resources/integration/PRE_2014.xml");
        Document documentDOM = XpathUtils.parse(fis);
        Locator location = new Locator(FILE, "/ClinicalDocument/title");
        try {
            XpathUtils.getStringValue(documentDOM, location, nsContext);
        } catch (GazelleXValidationException e) {
            Assert.assertTrue(e instanceof NoNodeSelectedException);
        }
    }

    @Test
    public void testGetDoubleWithNumber() throws FileNotFoundException, GazelleXValidationException {
        FileInputStream fis = new FileInputStream("src/test/resources/integration/PRE_2014.xml");
        Document documentDOM = XpathUtils.parse(fis);
        Locator location = new Locator(FILE, "10");
        try {
            Double value = XpathUtils.getDoubleValue(documentDOM, location, nsContext);
            System.out.println("value from locator: " + value);
            Assert.assertEquals((double) 10, value, 0);
        } catch (GazelleXValidationException e) {
            Assert.fail("Cannot select number");
        }
    }

    @Test
    public void testGetDoubleWithXpath() throws FileNotFoundException, GazelleXValidationException {
        FileInputStream fis = new FileInputStream("src/test/resources/integration/PRE_2014.xml");
        Document documentDOM = XpathUtils.parse(fis);
        Locator location = new Locator(FILE, "/*[name()='ClinicalDocument']/*[name()='author']/*[name()='assignedAuthor']/*[name()='addr']/*[name()" +
				"='postalCode']");
        try {
            Double value = XpathUtils.getDoubleValue(documentDOM, location, nsContext);
            System.out.println("value from locator: " + value);
            Assert.assertEquals((double) 1110, value, 0);
        } catch (GazelleXValidationException e) {
            Assert.fail("Cannot select element");
        } catch (NumberFormatException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSelectANodeAnyLevel() throws GazelleXValidationException {
        Document documentDOM = XpathUtils.parse(document);
        Locator locator = new Locator(FILE, "//v3:queryByParameter");
        DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, locator, nsContext);
        Assert.assertTrue("No node selected with XPath: " + locator, (nodes != null && nodes.getLength() > 0));
    }
}
