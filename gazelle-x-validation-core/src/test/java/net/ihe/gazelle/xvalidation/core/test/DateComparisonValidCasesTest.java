/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.test;

import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.report.Error;
import net.ihe.gazelle.xvalidation.core.report.Notification;
import net.ihe.gazelle.xvalidation.core.report.Report;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DateComparisonValidCasesTest {

    private static Locator locatorValidFormatWithDay;
    private static Locator locatorValidFormatWithMinutes;
    private static Locator locatorValidFormatWithSeconds;
    private static Locator locatorValidFormatWithFrenchTimezone;
    private static Locator locatorBeforeValidFormatWithFrenchTimezone;
    private static Locator locatorAfterValidFormatWithFrenchTimezone;
    private static Locator locatorValidFormatWithForeignTimezone;

    private static final String VALID_FORMAT_WITH_DAY = "validFormatWithDay";
    private static final String VALID_FORMAT_WITH_MINUTES = "validFormatWithMinutes";
    private static final String VALID_FORMAT_WITH_SECONDS = "validFormatWithSeconds";
    private static final String VALID_FORMAT_WITH_FRENCH_TIMEZONE = "validFormatWithFrenchTimezone";
    private static final String VALID_FORMAT_WITH_FOREIGN_TIMEZONE = "validFormatWithForeignTimezone";

    private static final DateFormatType WITH_TIMEZONE = new DateFormatType("WITH_TIMEZONE", "yyyyMMddHHmmssZ", 10);
    private static final DateFormatType TO_THE_SECOND = new DateFormatType("TO_THE_SECOND", "yyyyMMddHHmmss", 9);
    private static final DateFormatType TO_THE_MINUTE = new DateFormatType("TO_THE_MINUTE", "yyyyMMddHHmm", 8);
    private static final DateFormatType TO_THE_DAY = new DateFormatType("TO_THE_DAY", "yyyyMMdd", 7);

    private static HashMap<String, Node> files;
    private static ValidatorConfiguration validatorConfig;

    @BeforeClass
    public static void initClass() {

        locatorValidFormatWithDay = new Locator(VALID_FORMAT_WITH_DAY, "/*[name()='TestDocument']/*[name()='SameTime']/@value");
        locatorValidFormatWithMinutes = new Locator(VALID_FORMAT_WITH_MINUTES, "/*[name()='TestDocument']/*[name()='SameTime']/@value");
        locatorValidFormatWithSeconds = new Locator(VALID_FORMAT_WITH_SECONDS, "/*[name()='TestDocument']/*[name()='SameTime']/@value");
        locatorValidFormatWithFrenchTimezone = new Locator(VALID_FORMAT_WITH_FRENCH_TIMEZONE, "/*[name()='TestDocument']/*[name()='SameTime']/@value");
        locatorBeforeValidFormatWithFrenchTimezone = new Locator(VALID_FORMAT_WITH_FRENCH_TIMEZONE, "/*[name()='TestDocument']/*[name()='BeforeSameTime']/@value");
        locatorAfterValidFormatWithFrenchTimezone = new Locator(VALID_FORMAT_WITH_FRENCH_TIMEZONE, "/*[name()='TestDocument']/*[name()='AfterSameTime']/@value");
        locatorValidFormatWithForeignTimezone = new Locator(VALID_FORMAT_WITH_FOREIGN_TIMEZONE, "/*[name()='TestDocument']/*[name()='SameTime']/@value");
        files = new HashMap<>();
        InputStream validFormatWithDayFile = null;
        InputStream validFormatWithMinutesFile = null;
        InputStream validFormatWithSecondsFile = null;
        InputStream validFormatWithFrenchTimezoneFile = null;
        InputStream validFormatWithForeignTimezoneFile = null;
        try {
            validFormatWithDayFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithDay.xml");
            validFormatWithMinutesFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithMinutes.xml");
            validFormatWithSecondsFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithSeconds.xml");
            validFormatWithFrenchTimezoneFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithFrenchTimezone.xml");
            validFormatWithForeignTimezoneFile = new FileInputStream("src/test/resources/datecomparison/validFormatWithForeignTimezone.xml");
            files.put(VALID_FORMAT_WITH_DAY, XpathUtils.parse(validFormatWithDayFile));
            files.put(VALID_FORMAT_WITH_MINUTES, XpathUtils.parse(validFormatWithMinutesFile));
            files.put(VALID_FORMAT_WITH_SECONDS, XpathUtils.parse(validFormatWithSecondsFile));
            files.put(VALID_FORMAT_WITH_FRENCH_TIMEZONE, XpathUtils.parse(validFormatWithFrenchTimezoneFile));
            files.put(VALID_FORMAT_WITH_FOREIGN_TIMEZONE, XpathUtils.parse(validFormatWithForeignTimezoneFile));

            List<Namespace> namespaces = new ArrayList<>();
            namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
            validatorConfig = new ValidatorConfiguration();
            validatorConfig.setNamespaces(namespaces);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(validFormatWithDayFile);
            IOUtils.closeQuietly(validFormatWithMinutesFile);
            IOUtils.closeQuietly(validFormatWithSecondsFile);
            IOUtils.closeQuietly(validFormatWithFrenchTimezoneFile);
            IOUtils.closeQuietly(validFormatWithForeignTimezoneFile);
        }
    }

    @Test
    public void validTimezonesEqualComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorValidFormatWithForeignTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707123344+0200\"]\n" +
                "= " +
                "[validFormatWithForeignTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707173344+0700\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validTimezonesAfterComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.AFTER);
        comp.setLocator1(locatorAfterValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='AfterSameTime']/@value " +
                "matched String with value \"20200707123345+0200\"]\n" +
                "> " +
                "[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707123344+0200\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validTimezonesBeforeComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.BEFORE);
        comp.setLocator1(locatorBeforeValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='BeforeSameTime']/@value " +
                "matched String with value \"20200707123343+0200\"]\n" +
                "< " +
                "[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707123344+0200\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validTimezoneToMinutesComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithFrenchTimezone);
        comp.setLocator2(locatorValidFormatWithMinutes);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707123344+0200\"]\n" +
                "= " +
                "[validFormatWithMinutes::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"202007071033\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validDayToTimezoneComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithDay);
        comp.setLocator2(locatorValidFormatWithFrenchTimezone);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_DAY);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(WITH_TIMEZONE);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithDay::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707\"]\n" +
                "= " +
                "[validFormatWithFrenchTimezone::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707123344+0200\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validSecondsToMinutesComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithSeconds);
        comp.setLocator2(locatorValidFormatWithMinutes);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_SECOND);
        comp.setDateFormats1(dateFormatTypeList);

        dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_MINUTE);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithSeconds::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707103344\"]\n" +
                "= " +
                "[validFormatWithMinutes::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"202007071033\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }

    @Test
    public void validDayToDayComparisonTest() {
        DateComparison comp = new DateComparison(DateOperatorType.EQUAL);
        comp.setLocator1(locatorValidFormatWithDay);
        comp.setLocator2(locatorValidFormatWithDay);

        List<DateFormatType> dateFormatTypeList = new ArrayList<>();
        dateFormatTypeList.add(TO_THE_DAY);
        comp.setDateFormats1(dateFormatTypeList);
        comp.setDateFormats2(dateFormatTypeList);

        Notification result = comp.evaluate(files, validatorConfig, Level.ERROR);
        assertEquals("[validFormatWithDay::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707\"]\n" +
                "= " +
                "[validFormatWithDay::/*[name()='TestDocument']/*[name()='SameTime']/@value " +
                "matched String with value \"20200707\"]\n", result.getXpathReport());
        assertTrue("Test failed", result instanceof Report);
    }
}
